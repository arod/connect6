# Programs config
OUTDIR := bin
PROGS := MATCH EXERCISE CLIENT

CLIENT_NAME := c6client
CLIENT_FILES := client.cpp move_timer.cpp exec_interface.cpp net_interface.cpp
CLIENT_COMMON := string_buffer.cpp

MATCH_NAME := c6match
MATCH_FILES := match.cpp move_timer.cpp exec_interface.cpp
MATCH_COMMON := game.cpp

EXERCISE_NAME := lab9exercise
EXERCISE_FILES := lab9exercise.cpp move_timer.cpp exec_interface.cpp
EXERCISE_COMMON := game.cpp

# Tools config
CC := g++
LIBS := -lutil -lrt
CFLAGS := -std=c++0x $(if $(DEBUG),-g,)
INC := -Isrc/common -Isrc/client

# Functions
MAKE_INPUT = $(addprefix src/client/, $($(1)_FILES)) $(addprefix src/common/, $($(1)_COMMON))
MAKE_EXE = bin/$($(1)_NAME)

define MAKE_TARG
$(call MAKE_EXE,$(1)): $$(call MAKE_INPUT,$(1)) | $$(OUTDIR)
	$$(CC) -o $$@ $$(call MAKE_INPUT,$(1)) $$(CFLAGS) $$(INC) $$(LIBS)
endef

# Targets 
.DEFAULT_GOAL=all
.PHONY: clean all

EXES := $(foreach p,$(PROGS),$(call MAKE_EXE,$(p)))

all: $(EXES) 
clean:
	rm -f $(EXES)

$(foreach p,$(PROGS),$(eval $(call MAKE_TARG,$(p))))

