SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT=$SCRIPTDIR
C6M=$ROOT/c6match
LAB8=$ROOT/Lab8
LVLS="noobtrap blank ring center"

LAB9_C=/submit/aps105f/9/$USER/Lab9.c

if [ ! -e $LAB9_C ]
then
	echo Lab 9 submission for $USER not found. Aborting
	exit 1
fi

gcc -o Lab9 $LAB9_C --std=c99 -lm

if [ $? -ne 0 ]
then
	echo Error compiling Lab9 submission. Aborting
	exit 1
fi

if [ ! -x Lab9 ]
then
	echo Compiled Lab9 program not found in current directory. Aborting
	exit 1
fi

n=1
totalscore=0
lvlscores=

for lvl in $LVLS
do
	outfile=$n.out
	$C6M Lab8 $LAB8 $USER Lab9 $ROOT/$lvl.lvl 4 | tee $outfile

	result1=`tail -n 1 $outfile | awk '{print $5}'`
	result2=`tail -n 1 $outfile | awk '{print $9}'`

	lvlscore=0

	if [ $lvl == noobtrap ]
	then
		if [ $result1 == W ]; then lvlscore=`expr $lvlscore + 1`; fi
		if [ $result2 == W ]; then lvlscore=`expr $lvlscore + 2`; fi
		if [ $result2 == T ]; then lvlscore=`expr $lvlscore + 1`; fi
	else
		if [ $result2 == W ]; then lvlscore=`expr $lvlscore + 1`; fi
		if [ $result1 == W ]; then lvlscore=`expr $lvlscore + 2`; fi
		if [ $result1 == T ]; then lvlscore=`expr $lvlscore + 1`; fi
	fi

	lvlscore_fp=`awk "BEGIN{print $lvlscore / 2.0}"`
	lvlscores="$lvlscores $n: $lvlscore_fp/1.5"
	totalscore=`expr $totalscore + $lvlscore`

	n=`expr $n + 1`
done

totalscore_fp=`awk "BEGIN{print $totalscore / 2.0}"`

echo Marks: $lvlscores
echo Total: $totalscore_fp/6
echo Per-board results are in 1.out - 4.out

