#!/bin/bash

GEN=$1
OUT=$2
SCRIPT=~/connect6/scripts/play_list.sh

for slice in `seq 0 31`
do
	compnum=`expr $slice + 64`
	comp=p$compnum.ecf.utoronto.ca
	cmd1="\"$GEN | $SCRIPT $OUT $slice 32\""
	ssh $comp "screen -d -m bash -c $cmd1"
done
