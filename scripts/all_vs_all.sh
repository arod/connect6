#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"
SPAWNPROG=$SCRIPTDIR/spawn.sh
LISTPROG=$SCRIPTDIR/mklist.sh
INFILE=`readlink -f $1`
OUTFILE=`readlink -f $2`

rm -f $OUTFILE

$SPAWNPROG "$LISTPROG $INFILE" $OUTFILE

