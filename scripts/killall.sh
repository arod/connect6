#!/bin/bash

for slice in `seq 0 31`
do
	compnum=`expr $slice + 64`
	comp=p$compnum.ecf.utoronto.ca
	ssh $comp screen -X quit
done
