#!/usr/bin/python
import re
import sys
from collections import defaultdict

results = defaultdict(dict)
cats = ['W', 'L', 'WQ', 'LQ', 'WT', 'LT']

f = open(sys.argv[1], 'r')
for line in f:
	tok = line.split()
	lvl = tok[0]
	player = [ tok[1], tok[2] ]

	for p in player:
		if not results[p]:
			for c in cats:
				results[p][c] = 0

	games = [ [{}, {}], [{}, {}] ]

	for i in range(0,2):
		for j in range(0,2):
			result = tok[3+4*i+2*j]
			data = tok[3+4*i+2*j+1]
			games[i][j] = { 'result':result, 'data':data }

	for i in range(0,2):
		for j in range(0,2):
			result = games[i][j]['result']
			data = games[i][j]['data']

			if (result == 'N'): result = 'WQ'
			elif (result == 'Q'): result = 'LQ'
			elif (result == 'T'):
				myt = data
				othert = games[i][1-j]['data']

				if (myt < othert): result = 'WT'
				else: result = 'LT'

			p = player[j]
			results[p][result] += 1

f.close()

f = open(sys.argv[2], 'w')

f.write(",")
for c in cats:
	f.write('%s,' % c)
f.write("\n")

for p in results:
	f.write('%s,' % p)
	for c in cats:
		f.write('%s,' % results[p][c])

	f.write("\n")

f.close()


