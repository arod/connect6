#!/bin/bash
LVLS="noobtrap center ring"
INFILE=$1

for p1 in `cat $INFILE`
do
	for p2 in `cat $INFILE`
	do
		if [ !  "$p1" \< "$p2" ]
		then
			continue
		fi

		for v in $LVLS
		do
			echo $v $p1 $p2
		done
	done
done
