#!/usr/bin/python
import re
import operator
import sys
from collections import defaultdict

games = defaultdict(dict)

f = open(sys.argv[1], 'r')
for line in f:
	tok = line.split()
	lvl = tok[0]
	player = [ tok[1], tok[2] ]

	for i in range(0,2):
		if not games[player[i]]:
			games[player[i]] = defaultdict(dict) 
		if not games[player[i]][player[1-i]]:
			games[player[i]][player[1-i]] = defaultdict(dict)
		if not games[player[i]][player[1-i]][lvl]:
			games[player[i]][player[1-i]][lvl] = [] 

	for i in range(0,2):
		for j in range(0,2):
			result = tok[3+4*i+2*j]
			data = tok[3+4*i+2*j+1]
			entry = { 'result':result, 'data':data }
			games[player[j]][player[1-j]][lvl].append(entry)
f.close()

players = ['andreje2', 'wusihan1', 'xinwenbo', 'wangc148', 'sunsimi1', 'geisberg', 'tianpeif', 'weixun']


f = open(sys.argv[2], "w")
f.write("digraph tourn {\n")

for thisp in players:
	for otherp in players:
		if thisp == otherp: continue

		mywins = 0
		otherwins = 0
		myties = 0
		otherties = 0

		allgames = games[thisp][otherp]
		for lvl in allgames:
			for i in range(0,2):
				game = allgames[lvl][i]

				if game['result'] == 'W':
					mywins += 1
				elif game['result'] == 'L':
					otherwins += 1
				elif game['result'] == 'T':
					myt = float(game['data'])
					othert = float(games[otherp][thisp][lvl][i]['data'])

					if (myt < othert): myties += 1
					elif (othert < myt): otherties += 1


		if (mywins > otherwins):
			f.write('%s -> %s [label="%d"];\n' % (thisp, otherp, mywins-otherwins))
		elif (mywins == otherwins and myties > otherties):
			f.write('%s -> %s [label="%d" style=dotted];\n' % (thisp, otherp, myties-otherties))

f.write("}")
f.close()

