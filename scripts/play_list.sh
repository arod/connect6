#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Usage: $0 <outfile> [slice] [totalslices] (eats STDIN)"
	exit 1
fi

OUTFILE=$1
SLICE=$2
TOTALCPU=$3

if [ $# -lt 3 ]
then
	SLICE=0
	TOTALCPU=1
else
	OUTFILE=$OUTFILE.$SLICE
fi

SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT=$SCRIPTDIR/..
PLAYPROG=$ROOT/scripts/play.sh

rm -f $OUTFILE

outeridx=-1
while read line
do
	outeridx=`expr $outeridx + 1`
	if [ `expr $outeridx % $TOTALCPU` != $SLICE ]
	then
		continue
	fi

	lvl=`echo $line | awk '{print $1}'`
	p1=`echo $line | awk '{print $2}'`
	p2=`echo $line | awk '{print $3}'`

	echo $lvl `$PLAYPROG $p1 $p2 $lvl` | tee -a $OUTFILE
done
