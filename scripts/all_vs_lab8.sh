#!/bin/bash
SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"
PLAYPROG=$SCRIPTDIR/play8.sh
ROOT=$SCRIPTDIR/..
C6M=$ROOT/bin/c6match
PLAYERS=$ROOT/players
OUTFILE=$1
LVLS="blank ring noobtrap center"

rm -f $OUTFILE

for d in `ls $PLAYERS`
do
	for v in $LVLS
	do
		echo $v `$PLAYPROG $d $v` | tee -a $OUTFILE
	done
done
