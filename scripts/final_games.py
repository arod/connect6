#!/usr/bin/python
import re
import operator
import sys
from collections import defaultdict

games = defaultdict(dict)

f = open(sys.argv[1], 'r')
for line in f:
	tok = line.split()
	lvl = tok[0]
	player = [ tok[1], tok[2] ]

	for i in range(0,2):
		if not games[player[i]]:
			games[player[i]] = defaultdict(dict) 
		if not games[player[i]][player[1-i]]:
			games[player[i]][player[1-i]] = defaultdict(dict)
		if not games[player[i]][player[1-i]][lvl]:
			games[player[i]][player[1-i]][lvl] = [] 

	for i in range(0,2):
		for j in range(0,2):
			result = tok[3+4*i+2*j]
			data = tok[3+4*i+2*j+1]
			entry = { 'result':result, 'data':data }
			games[player[j]][player[1-j]][lvl].append(entry)
f.close()

players = ['andreje2', 'wusihan1', 'xinwenbo', 'wangc148', 'sunsimi1', 'geisberg', 'tianpeif', 'weixun']
scores = {}

for thisp in players:
	scores[thisp] = 0

	for otherp in players:
		if (thisp == otherp): continue

		allgames = games[thisp][otherp]
		for lvl in allgames:
			for i in range(0,2):
				code = allgames[lvl][i]['result']
				data = allgames[lvl][i]['data']

				if (code == 'W'):
					dscore = 2
				elif (code == 'Q'):
					dscore = -4
				elif (code == 'T'):
					mytime = float(data)
					othertime = float(games[otherp][thisp][lvl][i]['data'])
					dscore = 1.0 * othertime / (mytime + othertime)
				elif (code == 'L'):
					dscore = 0 
				else:
					dscore = 0

				scores[thisp] += dscore

f = open(sys.argv[2], 'w')

f.write(",,")
for p in players:
	f.write("%s,,,," % p)
f.write("\n")

for thisp in players:
	lvlrow = {}

	for otherp in players:
		if thisp == otherp: continue
		allgames = games[thisp][otherp]
		for lvl in allgames:
			if lvl not in lvlrow: lvlrow[lvl] = {}
			lvlrow[lvl][otherp] = allgames[lvl]

	for lvl in lvlrow:
		f.write("%s,%s," % (lvl, thisp))
		for otherp in players:
			if thisp == otherp:
				f.write(",,,,")
				continue
			for i in range(0,2):
				game=lvlrow[lvl][otherp]
				f.write("%s,%s," % (game[i]['result'], game[i]['data']))
		f.write("\n")
f.close()


