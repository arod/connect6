#!/usr/bin/python
import re
import operator
import sys
from collections import defaultdict

games = defaultdict(dict)

f = open(sys.argv[1], 'r')
for line in f:
	tok = line.split()
	lvl = tok[0]
	player = [ tok[1], tok[2] ]

	for i in range(0,2):
		if not games[player[i]]:
			games[player[i]] = defaultdict(dict) 
		if not games[player[i]][player[1-i]]:
			games[player[i]][player[1-i]] = defaultdict(dict)
		if not games[player[i]][player[1-i]][lvl]:
			games[player[i]][player[1-i]][lvl] = [] 

	for i in range(0,2):
		for j in range(0,2):
			result = tok[3+4*i+2*j]
			data = tok[3+4*i+2*j+1]
			entry = { 'result':result, 'data':data }
			games[player[j]][player[1-j]][lvl].append(entry)
f.close()

players = ['andreje2', 'wusihan1', 'xinwenbo', 'wangc148', 'sunsimi1', 'geisberg', 'tianpeif', 'weixun']

def perm(lst, func, arg=[]):
	if not lst:
		func(arg)
		return

	for i in range(0, len(lst)):
		arg_p = arg[:]
		arg_p.append(lst[i])
		lst_p = lst[:]
		del lst_p[i]
		perm(lst_p, func, arg_p)

def getwinner(p1, p2):
	allgames = games[p1][p2]

	p1w = 0
	p2w = 0
	p1t = 0
	p2t = 0

	for lvl in allgames:
		for i in range(0,2):
			game = allgames[lvl][i]

			if game['result'] == 'W':
				p1w += 1
			elif game['result'] == 'L':
				p2w += 1
			elif game['result'] == 'T':
				p1time = float(game['data'])
				p2time = float(games[p2][p1][lvl][i]['data'])

				if (p1time < p2time): p1t += 1
				elif (p2time < p1time): p2t += 1

	if p1w > p2w: return p1
	elif p2w > p1w: return p2
	elif (p1 == 'wangc148' and p2 == 'sunsimi1'):
		if p1t > p2t: return p1
	  	elif p2t > p1t: return p2	
	else: return ""

# Initialize slot counters
slots = [{}, {}, {}] 
for i in range(0,3):
	for p in players:
		slots[i][p] = 0

besttree=[]
ntrees = 0

# Plays adjacent pairs from a list
def playlist(lst, ptree = []):
	global besttree
	global ntrees

	lst_n = len(lst)
	tree = ptree[:]
	tree.extend(lst)

	if lst_n == 1:
		firstplace = tree[-1]
		secondplace = next(x for x in tree[-3:] if x != firstplace)
		bronze_in = [x for x in tree[-7:-3] if x not in [firstplace, secondplace]]
		thirdplace = getwinner(bronze_in[0], bronze_in[1])
		if thirdplace == "": return

		if firstplace != "wangc148": return
		if secondplace != "sunsimi1": return

		ntrees += 1
		slots[0][firstplace] += 1
		slots[1][secondplace] += 1
		slots[2][thirdplace] += 1

		topfirst = max(slots[0].iteritems(), key=operator.itemgetter(1))[0]
		topsecond = max(slots[1].iteritems(), key=operator.itemgetter(1))[0]
		topthird = max(slots[2].iteritems(), key=operator.itemgetter(1))[0]

		if firstplace==topfirst and secondplace==topsecond and thirdplace==topthird:
			besttree = tree

		return

	winners = []
	for i in range(0, lst_n, 2):
		p1 = lst[i]
		p2 = lst[i+1]
		winner = getwinner(p1, p2)
		if (winner == ""): return
		winners.append(winner)

	playlist(winners, tree)




perm (players, playlist)

print besttree
print ntrees
print slots[0]
print slots[1]
print slots[2]

		
sys.exit()

