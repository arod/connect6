#!/usr/bin/python
import re
import operator
import sys
from collections import defaultdict

games = {}
scores = {}

f = open(sys.argv[1], 'r')
for line in f:
	tok = line.split()
	lvl = tok[0]
	player = [ tok[1], tok[2] ]

	for i in range(0,2):
		thisp = player[i]
		otherp = player[1-i]
		scores[thisp] = 0
		if thisp not in games: 
			games[thisp] = {}
		if otherp not in games[thisp]: 
			games[thisp][otherp] = {}
		if lvl not in games[thisp][otherp]:
			games[thisp][otherp][lvl] = []

	for i in range(0,2):
		for j in range(0,2):
			result = tok[3+4*i+2*j]
			data = tok[3+4*i+2*j+1]
			entry = { 'result':result, 'data':data }
			games[player[j]][player[1-j]][lvl].append(entry)
f.close()

rankings = []

while len(scores) > 0:
	for thisp in scores:
		scores[thisp] = 0

		for otherp in scores:
			if (thisp == otherp): continue

			allgames = games[thisp][otherp]
			for lvl in allgames:
				for i in range(0,2):
					code = allgames[lvl][i]['result']
					data = allgames[lvl][i]['data']

					if (code == 'W'):
						dscore = 2
					elif (code == 'Q'):
						dscore = -4
					elif (code == 'T'):
						mytime = float(data)
						othertime = float(games[otherp][thisp][lvl][i]['data'])
						dscore = 1.0 * othertime / (mytime + othertime)
					elif (code == 'L'):
						dscore = 0 
					else:
						dscore = 0

					scores[thisp] += dscore

	scores_s = sorted(scores.items(), key=operator.itemgetter(1))
	loser = scores_s[0][0]
	del scores[loser]
	rankings.append(loser)
	print len(scores)
	
rankings.reverse()
for i in range(0,len(rankings)):
	print i, rankings[i]




