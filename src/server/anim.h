#pragma once

#include <SFML/System.hpp>
#include <vector>

namespace Anim
{
	class Timer
	{
	public:
		void set_for(int ms);
		float progress();
		bool done();

	private:
		sf::Clock m_clock;
		int m_duration;
	};

	class Sequence
	{
	public:
		void reset();
		bool done();
		virtual void draw() = 0;

	protected:
		struct StageInfo
		{
			int id;
			int duration;
		};
		typedef std::vector<StageInfo> StageInfos;

		void add_stage(int id, int duration, int after = -1);

		int m_cur_stage;
		Timer m_timer;
		StageInfos m_stages;
	};

	float scurve(float x);
	float paracurve(float x);
	float shake(float range = 1.0f);
}