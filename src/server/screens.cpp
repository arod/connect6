#include "common.h"
#include "static_init.h"
#include "screens.h"

using namespace Screens;

namespace
{
	typedef StaticRegistry<IScreen> RegisteredScreens;
	IScreen* s_screens[SCREEN_MAX];
	ScreenID s_cur_id;

	IScreen* cur_screen()
	{
		return s_screens[s_cur_id];
	}
}

void Screens::init()
{
	std::fill(std::begin(s_screens), std::end(s_screens), nullptr);

	for (auto& reg_func : RegisteredScreens::entries())
	{
		IScreen* scr = reg_func();
		ScreenID id = scr->get_id();
		s_screens[id] = scr;

		scr->on_init();
	}
	
	s_cur_id = LOBBY;
}

void Screens::shutdown()
{
	for (IScreen* scr : s_screens)
	{
		if (scr) 
		{
			scr->on_shutdown();
			delete scr;
		}
	}
}

void Screens::process()
{
	cur_screen()->process();
}

void Screens::goto_screen(ScreenID s)
{
	cur_screen()->on_leave();
	s_cur_id = s;
	cur_screen()->on_enter();
}

void Screens::handle_wnd_event(sf::Event& e)
{
	cur_screen()->wnd_event(e);
}