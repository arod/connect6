#include <SFML/Audio.hpp>
#include "gamestate.h"
#include "resource.h"
#include "client_interface.h"
#include "globals.h"

void GameState::init_new_game()
{
	// Tell client to start
	ClientInterface::clear_events();
	ClientInterface::start_game(this->level.size, this->cur_black, 
		this->level.walls);

	// Init board geometry
	Game::init_board(this->level);

	// Reset statistics
	this->n_moves = 0;
	this->player_msecs[0] = 0;
	this->player_msecs[1] = 0;

	// Reset move cursors
	for (int i = 0; i < 2; i++)
		this->cursors[i].reset();

	// Reset game speed
	gamespeed = Speed::NORMAL;
}

void GameState::init_new_match()
{
	// New match = new game with black as the first player
	this->cur_black = 0;
	init_new_game();
}

void GameState::accept_connection(ClientInterface::ConnectEvent* ce)
{
	this->player_names[0] = ce->player_names[0];
	this->player_names[1] = ce->player_names[1];

	try
	{
		std::string lvlfile = "levels/" + ce->suggested_map + ".lvl";
		this->level = Game::load_level(Resource::get_path(lvlfile));
		Game::init_board(this->level);
		init_new_match();

		if (!this->level.track.empty())
		{
			std::string musfile = "music/" + this->level.track + ".ogg";
			this->music.openFromFile(Resource::get_path(musfile));
			this->music.play();
		}
	}
	catch (Exception& e)
	{
		throw Exception("Failed loading level: " + std::string(e.what()));
	}
}

//
// Cursors
//

void GameState::Cursor::reset()
{
	state = NEWGAME;
}

void GameState::Cursor::init_move(int period, const Position& target)
{
	switch (state)
	{
		case NEWGAME:
			init_pos = target;
			cur_pos = target;
			target_pos = target;
			timer.set_for(period);
			state = MOVING;
			break;

		case IDLE:
			init_pos = cur_pos;
			timer.set_for(period);
			target_pos = target;
			state = MOVING;
			break;
	}
}

void GameState::Cursor::update()
{
	if (state != MOVING)
		return;

	// Delta for total movement from start to finish
	int drow = target_pos.row - init_pos.row;
	int dcol = target_pos.col - init_pos.col;
	bool row_is_major = drow > dcol;

	float total_progress = timer.progress();
	total_progress = Anim::scurve(total_progress);
	total_progress = total_progress * total_progress * total_progress;

	if (total_progress < 0.5f)
	{
		// Major axis first
		float axis_progress = total_progress * 2.0f;

		if (row_is_major)
		{
			cur_pos.row = (int)(init_pos.row + (target_pos.row - init_pos.row) * axis_progress + 0.5f);
		}
		else
		{
			cur_pos.col = (int)(init_pos.col + (target_pos.col - init_pos.col) * axis_progress + 0.5f);
		}
	}
	else
	{
		float axis_progress = (total_progress - 0.5f) * 2.0f;

		if (row_is_major)
		{
			cur_pos.row = target_pos.row;
			cur_pos.col = (int)(init_pos.col + (target_pos.col - init_pos.col) * axis_progress + 0.5f);
		}
		else
		{
			cur_pos.col = target_pos.col;
			cur_pos.row = (int)(init_pos.row + (target_pos.row - init_pos.row) * axis_progress + 0.5f);
		}
	}

	if (timer.done())
	{
		cur_pos = target_pos;
		state = DONE;
	}
}

bool GameState::Cursor::done()
{
	if (state == DONE)
	{
		state = IDLE;
		return true;
	}

	return false;
}

bool GameState::Cursor::exist()
{
	return state != NEWGAME;
}

int GameState::ms_per_move(GameState::Speed s)
{
	switch (s)
	{
		case Speed::PAUSED: return 100000;
		case Speed::SLOW: return 2000;
		case Speed::NORMAL: return 500;
		case Speed::FAST: return 125;
		case Speed::INSTANT: return 1;
	}

	return 1;
}

std::string GameState::speed_str(GameState::Speed s)
{
	switch (s)
	{
		case Speed::PAUSED: return "P";
		case Speed::SLOW: return ">";
		case Speed::NORMAL: return ">>";
		case Speed::FAST: return ">>>";
		case Speed::INSTANT: return ">>>>>";
	}

	return "unknown speed";
}
