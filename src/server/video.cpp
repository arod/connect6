#include <SFML/Graphics.hpp>
#include "video.h"
#include "globals.h"

using namespace Video;

namespace
{
	sf::RenderWindow* s_wnd;
}

void Video::init()
{
	sf::ContextSettings ctx_settings;
	ctx_settings.antialiasingLevel = 4;
	s_wnd = new sf::RenderWindow
	(
		sf::VideoMode(Globals::inst()->screen_width, Globals::inst()->screen_height),
		"Connect 6",
		sf::Style::Default,
		ctx_settings
	);
}

sf::RenderWindow* Video::get_window()
{
	return s_wnd;
}

void Video::shutdown()
{
	delete s_wnd;
}

void Video::draw_fade(float amt, const sf::Color& c)
{
	sf::RectangleShape r(
	{
		(float)s_wnd->getSize().x, 
		(float)s_wnd->getSize().y
	});

	sf::Color newc = c;
	newc.a = (sf::Uint8)(amt * 255.0f);

	r.setFillColor(newc);
	s_wnd->draw(r);
}


sf::Vector2f Video::text_size(const sf::Text& text)
{
	auto r = text.getLocalBounds();
	return sf::Vector2f(r.width, r.height);
}