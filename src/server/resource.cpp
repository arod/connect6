#include <unordered_map>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "resource.h"

using namespace Resource;

namespace
{
	const std::string DATA_DIR ="../data/";

	struct ResourceWrapperBase
	{
		virtual ~ResourceWrapperBase() {}
	};

	template <class T>
	struct ResourceWrapper : ResourceWrapperBase
	{
		ResourceWrapper(T* item) : ptr(item) { };
		~ResourceWrapper() { delete ptr; }
		T* ptr;
	};

	typedef std::unordered_map<std::string, ResourceWrapperBase*> ResourceMap;
	ResourceMap s_resources;


	sf::Font s_default_font;
}

void Resource::init()
{
	// Load common shared stuff
	//sf::Font* font = new sf::Font();
	//font->loadFromFile("../data/DejaVuSans.ttf");
	//add("default_font", font);

	s_default_font.loadFromFile("../data/DejaVuSans.ttf");
}

void Resource::shutdown()
{
	// Free all resources
	for (auto r : s_resources)
	{
		delete r.second;
	}
}

std::string Resource::get_path(const std::string& frag)
{
	return DATA_DIR + frag;
}


sf::Font& Resource::get_default_font() { return s_default_font; }


template<class T>
void Resource::add(const std::string& name, T* res)
{
	s_resources[name] = new ResourceWrapper<T>(res);
}

template<class T>
T& Resource::get(const std::string& name)
{
	auto wrap = (ResourceWrapper<T>*)s_resources[name];
	return (T&) (*wrap->ptr);
}

sf::Font& Resource::get_font(const std::string& name)
{
	return get<sf::Font>(name);
}

sf::Texture& Resource::get_tex(const std::string& name)
{
	return get<sf::Texture>(name);
}