#include <SFML/Graphics.hpp>
#include <unordered_map>

#include "draw_board.h"
#include "resource.h"
#include "video.h"
#include "globals.h"
#include "game.h"
#include "gamestate.h"

namespace
{
	struct Tileset
	{
		sf::Color bg1;
		sf::Color bg2;
		sf::Color wall;
	};

	sf::Texture s_black_tex;
	sf::Texture s_white_tex;

	bool s_do_cursors = false;
	float s_rot_progress = 0.0f;

	const sf::Color ACTIVE_CURSOR(255, 0, 255);
	//const sf::Color BLACK_CURSOR(255, 128, 0);
	//const sf::Color WHITE_CURSOR(


	const Tileset& get_tileset(const std::string& name)
	{
		static Tileset cold = { sf::Color(167, 177, 207), sf::Color(113, 89, 134), sf::Color(41, 41, 78) };
		static Tileset wood = { sf::Color(198, 168, 100), sf::Color(184, 111, 67), sf::Color(128, 0, 0) };
		static Tileset grass = { sf::Color(195, 216, 132), sf::Color(65, 112, 73), sf::Color(40, 25, 20) };
	
		if (name == "cold") return cold;
		else if (name == "wood") return wood;
		else if (name == "grass") return grass;
		else return wood;
	}
}

void DrawBoard::init()
{
	s_black_tex.loadFromFile(Resource::get_path("black_piece.png"));
	s_white_tex.loadFromFile(Resource::get_path("white_piece.png"));
	s_black_tex.setSmooth(true);
	s_white_tex.setSmooth(true);
}

void DrawBoard::enable_cursors(bool enable)
{
	s_do_cursors = enable;
}

void DrawBoard::set_rotation(float progress)
{
	s_rot_progress = progress;
}

void DrawBoard::draw()
{
	sf::RenderWindow* wnd = Video::get_window();
	sf::Font& font = Resource::get_default_font();
	GameState* gs = GameState::inst();

	const Tileset& tileset = get_tileset(gs->level.tileset);
	const sf::Color& col_bg1 = tileset.bg1;
	const sf::Color& col_bg2 = tileset.bg2;
	const sf::Color& col_wall = tileset.wall;

	// Set up board-coordinates to screen-coordinates transform
	// (only really need to recalc when screen resizes)
	//auto screen_size = s_wnd->getSize();
	sf::Vector2u screen_size(Globals::inst()->screen_width, Globals::inst()->screen_height);
	float board_pix = (float)screen_size.x * 3 / 4;
	board_pix = std::min(board_pix, (float)screen_size.y);
	float board_xpos = (screen_size.x - board_pix) / 2.0f;
	float board_ypos = (screen_size.y - board_pix) / 2.0f;
	int board_size = Game::get_board_size();

	sf::Transform board_xform;
	float anim = s_rot_progress;
	board_xform.translate(screen_size.x / 2.0f, screen_size.y / 2.0f);
	board_xform.rotate((1.0f - anim) * 90);
	board_xform.scale(4.0f - anim*3.0f, 4.0f - anim*3.0f);
	board_xform.translate(0 - screen_size.x / 2.0f, 0 - screen_size.y / 2.0f);
	board_xform.translate(board_xpos, board_ypos);
	board_xform.scale(board_pix / board_size, board_pix / board_size);

	// Draw background color 1 across whole board
	sf::RectangleShape bg_rect({ (float)board_size, (float)board_size });
	bg_rect.setFillColor(col_bg1);
	bg_rect.setPosition(0, 0);
	wnd->draw(bg_rect, board_xform);

	// Set up to draw alternating squares of background color 2
	bg_rect.setSize({ 1.0f, 1.0f });
	//bg_rect.setFillColor(COLOR_BG2);

	// Black and white piece sprite
	sf::Sprite piece;
	auto spr_size = s_black_tex.getSize();
	piece.setScale(0.8f / spr_size.x, 0.8f / spr_size.y);
	piece.setOrigin(spr_size.x / 2.0f, spr_size.y / 2.0f);

	for (int row = 0; row < board_size; row++)
	{
		for (int col = 0; col < board_size; col++)
		{
			if ((row ^ col) & 1)
			{
				bg_rect.setFillColor(col_bg2);
				bg_rect.setPosition((float)col, (float)row);
				wnd->draw(bg_rect, board_xform);
			}

			auto bval = Game::get_board(row, col);
			switch (bval)
			{
				case Game::BoardValue::BLACK:
				case Game::BoardValue::WHITE:
				{
					piece.setTexture(bval == Game::BoardValue::BLACK ?
						s_black_tex : s_white_tex
					);

					piece.setPosition((float)col + 0.5f, (float)row + 0.5f);
					wnd->draw(piece, board_xform);
					break;
				}

				case Game::BoardValue::WALL:
				{
					bg_rect.setFillColor(col_wall);
					bg_rect.setPosition((float)col, (float)row);
					wnd->draw(bg_rect, board_xform);
					break;
				}
			}
		}
	}

	if (s_do_cursors)
	{
		for (int i = 0; i < 2; i++)
		{
			GameState::Cursor& cursor = gs->cursors[i];

			if (cursor.exist())
			{
				Position& pos = cursor.cur_pos;
				bool is_black = i == gs->cur_black;
				const sf::Color& clr = is_black ? sf::Color::Black : sf::Color::White;

				sf::RectangleShape shp({ 1.00f, 1.00f });
				shp.setFillColor(sf::Color::Transparent);
				shp.setPosition((float)pos.col, (float)pos.row);
				shp.setOutlineColor(clr);
				shp.setOutlineThickness(0.1f);
				wnd->draw(shp, board_xform);
			}
		}
	}
}