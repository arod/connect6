#pragma once

namespace DrawBoard
{
	void init();

	void set_rotation(float progress);
	void enable_cursors(bool enable);

	void draw();
}