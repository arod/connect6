#pragma once

#include "common.h"
#include "game_types.h"

#undef ERROR

namespace ClientInterface
{
	struct Event
	{
		enum Type
		{
			CONNECT,
			PLAYER_DISCO,
			CLIENT_DISCO,
			GOT_MOVE,
			CLIENT_TIMEOUT,
			ERROR
		};

		Event(Type _type) : type(_type) { }
		virtual ~Event() { }
		virtual Event* clone() = 0;

		Type type;
	};

	struct ConnectEvent : public Event
	{
		ConnectEvent() : Event(Type::CONNECT) { }
		Event* clone() { return new ConnectEvent(*this); }
		std::string player_names[2];
		std::string suggested_map;
	};

	struct PlayerDisconnectEvent : public Event
	{
		PlayerDisconnectEvent() : Event(Type::PLAYER_DISCO) { }
		Event* clone() { return new PlayerDisconnectEvent(*this); }
		int player;
	};

	struct ClientDisconnectEvent : public Event
	{
		ClientDisconnectEvent() : Event(Type::CLIENT_DISCO) { }
		Event* clone() { return new ClientDisconnectEvent(*this); }
	};

	struct GotMoveEvent : public Event
	{
		GotMoveEvent() : Event(Type::GOT_MOVE) { }
		Event* clone() { return new GotMoveEvent(*this); }
		int player;
		Position position;
		double msec;
	};

	struct ClientTimeoutEvent : public Event
	{
		ClientTimeoutEvent() : Event(Type::CLIENT_TIMEOUT) { }
		Event* clone() { return new ClientTimeoutEvent(*this); }
		int player;
		double msec;
	};

	struct ErrorEvent : public Event
	{
		ErrorEvent(const std::string& s) : Event(Type::ERROR), what(s) { }
		Event* clone() { return new ErrorEvent(*this); }
		std::string what;
	};

	void init(unsigned short port);
	void shutdown();
	bool is_idle();
	bool is_connected();
	void process();
	Event* get_event();
	void listen();
	void disconnect();
	void get_next_move();
	void start_game(int board_size, int black_player, const Positions& walls);
	void clear_events();
}