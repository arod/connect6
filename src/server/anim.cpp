#include <cstdlib>
#include <SFML/System.hpp>
#include "anim.h"

using namespace Anim;

void Timer::set_for(int ms)
{
	m_duration = ms;
	m_clock.restart();
}

float Timer::progress()
{
	int elapsed = m_clock.getElapsedTime().asMilliseconds();
	return (float)elapsed / m_duration;
}

bool Timer::done()
{
	return progress() >= 1.0f;
}

float Anim::scurve(float x)
{
	float xx = x*x;
	return 3.0f*xx - 2.0f*xx*x;
}

float Anim::paracurve(float x)
{
	x -= 1.0f;
	return -(x*x) + 1.0f;
}

float Anim::shake(float range)
{
	// between -1 and 1
	return ((float)rand() / (float)(RAND_MAX/2) - 1.0f) * range;
}