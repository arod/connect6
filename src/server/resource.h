#pragma once
#include <string>

namespace sf
{
	class Font;
	class Texture;
}

namespace Resource
{
	void init();
	void shutdown();

	std::string get_path(const std::string& filename);

	template<class T>
	T& get(const std::string& name);

	template<class T>
	void add(const std::string& name, T* res);

	sf::Font& get_font(const std::string& name);
	sf::Texture& get_tex(const std::string& name);

	sf::Font& get_default_font();
}