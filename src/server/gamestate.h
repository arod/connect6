#pragma once

#include <string>
#include <SFML/Audio.hpp>
#include "game.h"
#include "client_interface.h"
#include "anim.h"

struct GameState
{
	struct Cursor
	{
		Position init_pos;
		Position cur_pos;
		Position target_pos;
		Anim::Timer timer;

		enum
		{
			NEWGAME,
			IDLE,
			MOVING,
			DONE
		} state;

		void reset();
		void update();
		void init_move(int period, const Position& target);
		bool done();
		bool exist();
	};

	enum class Speed
	{
		PAUSED,
		SLOW,
		NORMAL,
		FAST,
		INSTANT
	};

	int cur_black;
	std::string player_names[2];
	Game::LevelInfo level;

	int active_player;
	int n_moves;
	double player_msecs[2];
	double last_time;
	Cursor cursors[2];
	Speed gamespeed;

	sf::Music music;

	// Call this to get the singleton
	static GameState* inst()
	{
		static GameState s;
		return &s;
	}

	void accept_connection(ClientInterface::ConnectEvent*);
	void init_new_game();
	void init_new_match();

	int ms_per_move(Speed s);
	std::string speed_str(Speed s);

protected:
	GameState() = default;
};

