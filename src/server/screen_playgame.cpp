#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <deque>
#include "screens.h"
#include "static_init.h"
#include "game.h"
#include "common.h"
#include "video.h"
#include "client_interface.h"
#include "globals.h"
#include "resource.h"
#include "anim.h"
#include "gamestate.h"
#include "draw_board.h"
#include "draw_statusbar.h"

namespace
{
	const int CURSOR_MS = 450;
	const int BETWEEN_MOVES_MS = 175;

	class PlayGameScreen : public Screens::IScreen
	{
		void on_init();
		void on_shutdown() { }
		void process();
		void wnd_event(sf::Event& e);
		void on_enter();
		void on_leave();
		Screens::ScreenID get_id() { return Screens::INGAME; }
	};

	struct MovSeqState
	{
		enum
		{
			GET_NEXT_MOVE,
			WAIT_FOR_NET,
			START_ANIM,
			WAIT_FOR_ANIM,
			PLACE_PIECE,
			PAUSE_BETWEEN_MOVES
		} phase;

		Position final_pos;
		PlayerColor player_color;
		int player_idx;
	} s_move_seq;

	enum class State
	{
		X_VS_Y,
		SWITCH,
		Y_VS_X,
		RESULT,
		OUTRO
	} s_cur_state;

	struct GameResultData
	{
		enum Type
		{
			COMPLETE,
			ERROR,
			BAD_MOVE
		} type;

		Game::GameStatus complete_result;
		ClientInterface::Event* incomplete_result;
	} s_game_result;

	Screens::Register<PlayGameScreen> s_reg;
	Anim::Timer s_timer;
	std::deque<sf::Event> s_wnd_events;
	
	void render();
	bool get_key(sf::Keyboard::Key* out = 0);
	bool process_game_net();
	bool process_game_keys();
	void process_game();
	void process_switch();
	void render_game();
	void process_result();
	void render_result();
}

namespace
{
	bool get_key(sf::Keyboard::Key* out)
	{
		bool result = false;

		while (!s_wnd_events.empty() && !result)
		{
			auto event = s_wnd_events.front();
			s_wnd_events.pop_front();

			switch (event.type)
			{
				case sf::Event::KeyPressed:
					if (out)
					{
						*out = event.key.code;
					}
					result = true;
					break;
			}
		}

		return result;
	}

	//
	// API hooks
	//

	void PlayGameScreen::on_init()
	{
	}

	void PlayGameScreen::wnd_event(sf::Event& event)
	{
		s_wnd_events.push_back(event);
	}

	void PlayGameScreen::on_leave()
	{
	}

	void PlayGameScreen::on_enter()
	{
		DrawBoard::enable_cursors(true);
		DrawBoard::set_rotation(1.0f);

		// new game already initialized when connection was accepted
		s_cur_state = State::X_VS_Y;
		s_move_seq.phase = MovSeqState::GET_NEXT_MOVE;
	}

	void PlayGameScreen::process()
	{
		switch (s_cur_state)
		{
			case State::SWITCH:
				process_switch();
				break;

			case State::X_VS_Y:
			case State::Y_VS_X:
				process_game();
				break;

			case State::RESULT:
				process_result();
				break;
		}

		render();
	}

	void render()
	{
		sf::RenderWindow* wnd = Video::get_window();

		wnd->clear();

		switch (s_cur_state)
		{
			case State::X_VS_Y:
			case State::Y_VS_X:
				render_game();
				break;

			case State::SWITCH:
				break;

			case State::RESULT:
				render_result();
				break;
		}

		wnd->display();
	}

	//
	// GAME
	//

	bool process_game_keys()
	{
		GameState* gs = GameState::inst();

		// Do keys
		sf::Keyboard::Key key;
		if (get_key(&key))
		{
			switch (key)
			{
				case sf::Keyboard::Numpad0: gs->gamespeed = GameState::Speed::PAUSED; break;
				case sf::Keyboard::Numpad1: gs->gamespeed = GameState::Speed::SLOW; break;
				case sf::Keyboard::Numpad2: gs->gamespeed = GameState::Speed::NORMAL; break;
				case sf::Keyboard::Numpad3: gs->gamespeed = GameState::Speed::FAST; break;
				case sf::Keyboard::Numpad5: gs->gamespeed = GameState::Speed::INSTANT; break;
			}
		}

		return false;
	}

	void process_game()
	{
		GameState* gs = GameState::inst();
		bool game_over = false;

		switch (s_move_seq.phase)
		{
			case MovSeqState::GET_NEXT_MOVE:
			{
				ClientInterface::get_next_move();
				s_move_seq.phase = MovSeqState::WAIT_FOR_NET;
				break;
			}
			case MovSeqState::WAIT_FOR_NET:
			{
				game_over = process_game_net();
				break;
			}

			case MovSeqState::START_ANIM:
			{
				auto spd = gs->gamespeed;
				if (spd != GameState::Speed::PAUSED)
				{
					int movetime = gs->ms_per_move(spd);
					gs->cursors[s_move_seq.player_idx].init_move(
						movetime, s_move_seq.final_pos);
					s_move_seq.phase = MovSeqState::WAIT_FOR_ANIM;
				}

				break;
			}

			case MovSeqState::WAIT_FOR_ANIM:
			{
				int player = s_move_seq.player_idx;
				GameState::Cursor& cursor = gs->cursors[player];
				cursor.update();

				if (cursor.done())
				{
					s_move_seq.phase = MovSeqState::PLACE_PIECE;
				}

				break;
			}

			case MovSeqState::PLACE_PIECE:
			{
				PlayerColor color = s_move_seq.player_color;
				const Position& move = s_move_seq.final_pos;
				Game::play_move(color, move);

				auto status = Game::get_status();
				if (status != Game::GameStatus::IN_PROGRESS)
				{
					s_game_result.type = GameResultData::COMPLETE;
					s_game_result.complete_result = status;
					game_over = true;
					break;
				}

				s_timer.set_for(BETWEEN_MOVES_MS);
				s_move_seq.phase = MovSeqState::PAUSE_BETWEEN_MOVES;

				break;
			}

			case MovSeqState::PAUSE_BETWEEN_MOVES:
			{

				if (s_timer.done() || gs->gamespeed == GameState::Speed::INSTANT)
					s_move_seq.phase = MovSeqState::GET_NEXT_MOVE;

				break;
			}
		}

		game_over |= process_game_keys();

		if (game_over)
		{
			if (gs->cur_black == 1)
			{
				gs->cur_black = 1;
			}

			s_timer.set_for(3000);
			s_cur_state = State::RESULT;
		}
	}

	void process_switch()
	{
		GameState* gs = GameState::inst();
		gs->cur_black = 1 - gs->cur_black;
		gs->init_new_game();

		s_cur_state = State::Y_VS_X;
		s_move_seq.phase = MovSeqState::GET_NEXT_MOVE;
	}

	bool process_game_net()
	{
		using namespace ClientInterface;

		ClientInterface::Event* e = ClientInterface::get_event();
		if (!e)
			return false;
		
		GameState* gs = GameState::inst();
		bool game_over = false;

		switch (e->type)
		{
			case Event::GOT_MOVE:
			{
				auto me = (GotMoveEvent*)e;

				PlayerColor color = me->player == gs->cur_black ? P_BLACK : P_WHITE;

				bool valid = Game::is_valid_move(color, me->position);
					
				gs->last_time = me->msec;
				gs->n_moves++;
				gs->player_msecs[me->player] += me->msec;
				gs->active_player = me->player;

				if (!valid)
				{
					s_game_result.type = GameResultData::BAD_MOVE;
					game_over = true;
					break;
				}

				s_move_seq.final_pos = me->position;
				s_move_seq.player_color = color;
				s_move_seq.player_idx = me->player;
					
				s_move_seq.phase = MovSeqState::START_ANIM;

				break;
			}

			case Event::CLIENT_TIMEOUT:
			case Event::PLAYER_DISCO:
			case Event::ERROR:
			{
				ClientInterface::disconnect();
				s_game_result.type = GameResultData::ERROR;
				s_game_result.incomplete_result = e->clone();
				game_over = true;
				break;
			}

			case Event::CLIENT_DISCO:
			{
				Screens::goto_screen(Screens::LOBBY);
				game_over = true;
				break;
			}
		}

		delete e;
		return game_over;
	}

	void render_game()
	{
		DrawBoard::draw();
		DrawStatusBar::draw();
	}

	//
	// GAME RESULT
	//

	void render_result()
	{
		DrawBoard::draw();
		DrawStatusBar::draw();

		sf::RenderWindow* wnd = Video::get_window();
		sf::Font& font = Resource::get_default_font();
		GameState* gs = GameState::inst();

		std::string res_str;

		int b_idx = gs->cur_black;
		int w_idx = 1 - gs->cur_black;
		std::string& b_name = gs->player_names[b_idx];
		std::string& w_name = gs->player_names[w_idx];
		int win_idx = -1;
		
		switch (s_game_result.type)
		{
			case GameResultData::COMPLETE:
			{
				switch (s_game_result.complete_result)
				{
					using namespace Game;

					case GameStatus::WHITE_WINS:
						res_str = w_name + " wins in " + to_string(gs->n_moves) + " moves!";
						break;
					case GameStatus::BLACK_WINS:
						res_str = b_name + " wins in " + to_string(gs->n_moves) + " moves!";
						break;

					case GameStatus::TIE:
						res_str = "Draw! ";
						break;
				}
			}
			break;

			case GameResultData::BAD_MOVE:
			{
				auto me = (ClientInterface::GotMoveEvent*)s_game_result.incomplete_result;
				res_str = gs->player_names[me->player];
				res_str += " played an invalid move at (" +
					to_string(me->position.row) +
					" " + to_string(me->position.col) +
					")";
			}
			break;

			case GameResultData::ERROR:
				switch (s_game_result.incomplete_result->type)
				{
					using namespace ClientInterface;
					case Event::CLIENT_TIMEOUT:
					{
						auto te = (ClientTimeoutEvent*)s_game_result.incomplete_result;
						res_str = gs->player_names[te->player];
						res_str += " timed out in " +
							to_string(te->msec) + " msec";
					}
					break;

					case Event::PLAYER_DISCO:
					{
						auto de = (PlayerDisconnectEvent*)s_game_result.incomplete_result;
						res_str = gs->player_names[de->player];
						res_str += " quit unexpectedly";
					}
					break;

					case Event::ERROR:
					{
						auto ee = (ErrorEvent*)s_game_result.incomplete_result;
						res_str = "Error: " + ee->what;
					}
					break;
				}
		}

		sf::Text res_txt(res_str, font);
		res_txt.setCharacterSize(60);
		res_txt.setStyle(sf::Text::Bold);

		res_txt.setColor(sf::Color(sf::Color::Black));
		res_txt.setPosition(100, 250-4);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 250+4);
		wnd->draw(res_txt);
		res_txt.setPosition(97, 250);
		wnd->draw(res_txt);
		res_txt.setPosition(103, 250);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 250);
		res_txt.setColor(sf::Color(sf::Color::White));
		wnd->draw(res_txt);

		std::string timetext;

		timetext = gs->player_names[0] + ": " + to_string(gs->player_msecs[0]) + "ms";
		res_txt.setString(timetext);

		res_txt.setColor(sf::Color(sf::Color::Black));
		res_txt.setPosition(100, 425-4);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 425+4);
		wnd->draw(res_txt);
		res_txt.setPosition(97, 425);
		wnd->draw(res_txt);
		res_txt.setPosition(103, 425);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 425);
		res_txt.setColor(sf::Color(sf::Color::White));
		wnd->draw(res_txt);


		timetext = gs->player_names[1] + ": " + to_string(gs->player_msecs[1]) + "ms";
		res_txt.setString(timetext);
		
		res_txt.setColor(sf::Color(sf::Color::Black));
		res_txt.setPosition(100, 600 - 4);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 600 + 4);
		wnd->draw(res_txt);
		res_txt.setPosition(97, 600);
		wnd->draw(res_txt);
		res_txt.setPosition(103, 600);
		wnd->draw(res_txt);
		res_txt.setPosition(100, 600);
		res_txt.setColor(sf::Color(sf::Color::White));
		wnd->draw(res_txt);
	}

	void process_result()
	{
		GameState* gs = GameState::inst();

		if (s_timer.done() && get_key())
		{
			if (s_game_result.type != GameResultData::COMPLETE)
			{
				delete s_game_result.incomplete_result;
				ClientInterface::disconnect();
				Screens::goto_screen(Screens::LOBBY);
				return;
			}

			// First, or second game?
			if (gs->cur_black == 0)
			{
				s_wnd_events.clear();
				s_cur_state = State::SWITCH;
			}
			else
			{
				ClientInterface::disconnect();
				Screens::goto_screen(Screens::LOBBY);
			}
		}
		else
		{
			s_wnd_events.clear();
		}
	}
}
