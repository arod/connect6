#pragma once

#include "game_types.h"

namespace sf { class RenderWindow; }

namespace Render
{
	void init();
	void shutdown();

	void draw_board(const Position& highlight);
}