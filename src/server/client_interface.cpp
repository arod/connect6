#include <unordered_map>
#include <functional>
#include <deque>
#include <SFML/Network.hpp>
#include "client_interface.h"
#include "string_buffer.h"

using namespace ClientInterface;

//
// Private declarations
//

namespace
{
	static const int PROTO_VERSION = 8;
	static const int TIMEOUT_MS = 1050;
	
	// State definitions for internal state machine
	enum class State
	{
		IDLE,		//
		LISTENING,	// waiting for client to connect
		CONNECTED	// steady state - waiting for server code to tell us to do network stuff
	};

	void push_error(const std::string& err);
	void push_event(Event* e);
	void check_new_connections();
	void recv_new_data();
	void parse_client_packet();
	void parse_hello_cmd(char* buf);
	void parse_disco_cmd(char* buf);
	void parse_move_cmd(char* buf);
	void send_cmd(const std::string& cmd);

	State s_cur_state;
	sf::TcpListener s_listener;
	sf::TcpSocket s_socket;
	std::deque<Event*> s_event_queue;
	StringBuffer s_recv_buffer;
	unsigned short s_port;
}

//
// Private function bodies
//

namespace
{
	void push_event(Event* e)
	{
		s_event_queue.push_back(e);
	}

	void check_new_connections()
	{
		auto result = s_listener.accept(s_socket);
		// If no incoming connection, get out (this is nonblocking)
		if (result != sf::Socket::Done)
			return;

		// Successful connection, move to next state
		s_recv_buffer.reset();
		s_cur_state = State::CONNECTED;
	}
	
	void send_cmd(const std::string& cmd)
	{
		const char* data = cmd.c_str();
		size_t size = cmd.size() + 1; // include null

		auto status = s_socket.send(data, size);
		switch (status)
		{
			case sf::Socket::Done:
				break;

			case sf::Socket::Disconnected:
				push_event(new ClientDisconnectEvent());
				s_cur_state = State::IDLE;
				s_socket.disconnect();
				s_listener.close();
				break;

			default:
				push_error("Failed send");
				break;
		}
	}

	void recv_new_data()
	{
		// Buffer any incoming data from the socket
		size_t bytes = s_recv_buffer.get_write_space();
		char* buffer = s_recv_buffer.get_write_ptr();

		size_t actual_bytes;
		auto status = s_socket.receive(buffer, bytes, actual_bytes);

		switch (status)
		{
			case sf::Socket::NotReady:
				// no data, no problem
				return;

			case sf::Socket::Disconnected:
				// remote side disconnected
				push_event(new ClientDisconnectEvent());
				s_socket.disconnect();
				s_listener.close();
				s_cur_state = State::IDLE;
				return;

			case sf::Socket::Done:
				// data successfully received
				break;

			default:
				// anything else is an error
				push_error("Recv failed");
				return;
		}

		// tell our buffer to advance its write pointer
		s_recv_buffer.buffer_data(actual_bytes);
	}

	void parse_hello_cmd(const char* buf)
	{
		int proto_version;

		// Get protocol version. Note: %n makes scanf return number of chars scanned from start of buf
		int delta;
		int nargs = sscanf(buf, " proto %d%n", &proto_version, &delta);
		if (nargs != 1)
		{
			push_error("Bad hello command");
			return;
		}

		if (proto_version != PROTO_VERSION)
		{
			push_error("Got protocol version " + std::to_string(proto_version) +
				" but expected " + std::to_string(PROTO_VERSION));
			return;
		}

		buf += delta;

		// Get player name strings and "suggested level" string
		std::string player0_name;
		std::string player1_name;
		std::string level_name;

		std::string* strings[] = { &player0_name, &player1_name, &level_name };

		for (auto str : strings)
		{
			char strbuf[64];
			nargs = sscanf(buf, "%63s%n", strbuf, &delta);

			if (nargs != 1)
				push_error("Bad hello command");

			str->assign(strbuf);
			buf += delta;
		}

		// Create connection event and add to event queue
		ConnectEvent* ev = new ConnectEvent();
		ev->player_names[0] = player0_name;
		ev->player_names[1] = player1_name;
		ev->suggested_map = level_name;
		push_event(ev);
	}

	void parse_disco_cmd(const char* buf)
	{
		int player;

		int nargs = sscanf(buf, "%d", &player);
		if (nargs != 1 || (player != 0 && player != 1))
		{
			push_error("Bad player_disco command");
			return;
		}

		PlayerDisconnectEvent* ev = new PlayerDisconnectEvent();
		ev->player = player;
		push_event(ev);
	}

	void parse_move_cmd(const char* buf)
	{
		Position pos;
		int player;
		double msec;

		int nargs = sscanf(buf, "%d %d %d %lf", &player, &pos.row, &pos.col, &msec);
		if (nargs != 4 || (player != 0 && player != 1))
		{
			push_error("Bad move command");
			return;
		}

		if (msec > (double)TIMEOUT_MS)
		{
			ClientTimeoutEvent* ev = new ClientTimeoutEvent();
			ev->player = player;
			ev->msec = msec;
			push_event(ev);
		}
		else
		{
			GotMoveEvent* ev = new GotMoveEvent();
			ev->player = player;
			ev->position = pos;
			ev->msec = msec;
			push_event(ev);
		}
	}

	void push_error(const std::string& err)
	{
		push_event(new ErrorEvent(err));
		s_socket.disconnect();
		s_cur_state = State::IDLE;
	}

	void parse_client_packet()
	{
		// Need a complete null-terminated string in the buffer
		if (!s_recv_buffer.ready())
			return;

		// Get string
		const std::string& cmd = s_recv_buffer.get_string();

		// Register commands.
		// Need to use lambdas because these functions aren't global/static
		static std::unordered_map<std::string, std::function<void(const char*)>> cmds =
		{
			{ "hello", [=](const char* b) {parse_hello_cmd(b); } },
			{ "disco", [=](const char* b) {parse_disco_cmd(b); } },
			{ "move", [=](const char* b) {parse_move_cmd(b); } }
		};

		// Extract the command and look it up, finding the handler
		int cmd_len = cmd.find_first_of(' ');
		std::string cmd_code = cmd.substr(0, cmd_len);

		if (cmds.count(cmd_code) > 0)
		{
			// Handler exists. Call it.
			auto& cmd_handler = cmds[cmd_code];

			// Skip space and pass arguments to handler
			const char* args = cmd.c_str() + cmd_len + 1;
			cmd_handler(args);
		}
		else
		{
			push_error("Unknown client command: " + cmd);
			return;
		}

		// Tell the buffer we've consumed the string
		s_recv_buffer.consume_string();
	}
}

//
// Public methods
//

void ClientInterface::init(unsigned short port)
{
	s_port = port;
	s_cur_state = State::IDLE;
}

void ClientInterface::shutdown()
{
	disconnect();
}

void ClientInterface::disconnect()
{
	if (s_cur_state == State::CONNECTED)
	{
		send_cmd("disconnect");
	}

	s_socket.disconnect();
	s_listener.close();
	s_cur_state = State::IDLE;
}

void ClientInterface::get_next_move()
{
	send_cmd("get_move");
}

ClientInterface::Event* ClientInterface::get_event()
{
	if (s_event_queue.empty())
		return nullptr;
	
	Event* result = s_event_queue.front();
	s_event_queue.pop_front();
	return result;
}

void ClientInterface::process()
{
	switch (s_cur_state)
	{
		case State::IDLE:
			break;

		case State::LISTENING:
			check_new_connections();
			break;

		case State::CONNECTED:
			recv_new_data();
			parse_client_packet();
			break;

		default:
			assert(false);
			break;
	}
}

void ClientInterface::listen()
{
	if (s_cur_state != State::IDLE)
	{
		s_socket.disconnect();
		s_listener.close();
	}

	s_listener.setBlocking(false);
	auto status = s_listener.listen(s_port);

	if (status != sf::Socket::Done)
	{
		push_event(new ErrorEvent("listening on socket"));
		s_cur_state = State::IDLE;
		return;
	}

	s_socket.setBlocking(false);
	s_cur_state = State::LISTENING;
}

bool ClientInterface::is_idle()
{
	return s_cur_state == State::IDLE;
}

bool ClientInterface::is_connected()
{
	return s_cur_state == State::CONNECTED;
}

void ClientInterface::start_game(int board_size, int black_player, const Positions& walls)
{
	std::string cmd = "start_game size " + to_string(board_size) +
		" black " + to_string(black_player) +
 		" nwalls " + to_string(walls.size());

	for (auto& wall : walls)
	{
		cmd += " " + to_string(wall.row) +
			" " + to_string(wall.col);
	}

	send_cmd(cmd);
}

void ClientInterface::clear_events()
{
	s_recv_buffer.reset();
	while (Event* e = get_event())
	{
		delete e;
	}
}


