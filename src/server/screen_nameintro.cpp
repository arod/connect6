#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "screens.h"
#include "game.h"
#include "common.h"
#include "video.h"
#include "client_interface.h"
#include "globals.h"
#include "resource.h"
#include "anim.h"
#include "gamestate.h"

namespace
{
	const int INTRO_NAMES_SIZE = 80;
	const int INTRO_VS_SIZE = 40;

	class NameIntroScreen : public Screens::IScreen
	{
		void on_init();
		void on_shutdown() { }
		void process();
		void wnd_event(sf::Event& e);
		void on_enter();
		void on_leave();
		Screens::ScreenID get_id() { return Screens::NAME_INTRO; }
	};

	Screens::Register<NameIntroScreen> s_reg;

	enum class State
	{
		ENTRY,
		NAME1_FLY,
		NAME1_SHAKE,
		VS_ENTER,
		VS_HOVER,
		VS_LEAVE,
		NAME2_FLY,
		NAME2_SHAKE,
		POSITIONS,
		EXIT
	} s_cur_state;

	Anim::Timer s_timer;
	
	void process_nameintro();
	void render_nameintro();
}

namespace
{
	//
	// API hooks
	//

	void NameIntroScreen::on_init()
	{
	}

	void NameIntroScreen::wnd_event(sf::Event& event)
	{
		if (event.type == sf::Event::KeyPressed)
		{
			s_cur_state = State::EXIT;
		}
	}

	void NameIntroScreen::on_leave()
	{
	}

	void NameIntroScreen::on_enter()
	{
		s_cur_state = State::ENTRY;
	}

	void NameIntroScreen::process()
	{
		process_nameintro();
		render_nameintro();
	}

	//
	// Private
	//

	void process_nameintro()
	{
		switch (s_cur_state)
		{
			case State::ENTRY:
				s_timer.set_for(200);
				s_cur_state = State::NAME1_FLY;
				break;

			case State::NAME1_FLY:
				if (s_timer.done())
				{
					s_cur_state = State::NAME1_SHAKE;
					s_timer.set_for(400);
				}
				break;

			case State::NAME1_SHAKE:
				if (s_timer.done())
				{
					s_cur_state = State::VS_ENTER;
					s_timer.set_for(500);
				}
				break;

			case State::VS_ENTER:
				if (s_timer.done())
				{
					s_cur_state = State::VS_HOVER;
					s_timer.set_for(200);
				}
				break;

			case State::VS_HOVER:
				if (s_timer.done())
				{
					s_cur_state = State::NAME2_FLY;
					s_timer.set_for(200);
				}
				break;

			case State::NAME2_FLY:
				if (s_timer.done())
				{
					s_cur_state = State::NAME2_SHAKE;
					s_timer.set_for(400);
				}
				break;

			case State::NAME2_SHAKE:
				if (s_timer.done())
				{
					s_cur_state = State::VS_LEAVE;
					s_timer.set_for(500);
				}
				break;

			case State::VS_LEAVE:
				if (s_timer.done())
				{
					s_cur_state = State::POSITIONS;
					s_timer.set_for(750);
				}
				break;

			case State::POSITIONS:
				if (s_timer.done())
				{
					s_cur_state = State::EXIT;
					s_timer.set_for(100);
				}
				break;

			case State::EXIT:
				if (s_timer.done())
				{
					Screens::goto_screen(Screens::BOARD_INTRO);
				}
				break;
		}
	}

	void render_nameintro()
	{
		sf::RenderWindow* wnd = Video::get_window();
		sf::Font& font = Resource::get_default_font();

		auto wndsize = wnd->getSize();

		const std::string p1 = GameState::inst()->player_names[0];
		const std::string p2 = GameState::inst()->player_names[1];

		sf::Text p1name(p1, font);
		sf::Text p2name(p2, font);
		sf::Text vs("VS", font);

		p1name.setCharacterSize(INTRO_NAMES_SIZE);
		p2name.setCharacterSize(INTRO_NAMES_SIZE);
		vs.setCharacterSize(INTRO_VS_SIZE);

		auto p1size = Video::text_size(p1name);
		auto p2size = Video::text_size(p2name);
		auto vs_size = Video::text_size(vs);

		float total_h = p1size.y + p2size.y + vs_size.y * 2;
		float p1y = (wndsize.y - total_h) / 2;
		float vs_y = p1y + p1size.y + vs_size.y / 2;
		float p2y = vs_y + vs_size.y + vs_size.y / 2;

		float p1x = (wndsize.x - p1size.x) / 2;
		float vs_x = (wndsize.x - vs_size.x) / 2;
		float p2x = (wndsize.x - p2size.x) / 2;

		float ofs = s_timer.progress();

		wnd->clear();

		switch (s_cur_state)
		{
			case State::NAME1_FLY:
				ofs *= ofs;
				p1name.setOrigin({ p1size.x / 2, p1size.y / 2 });
				p1name.setPosition(p1x + p1size.x / 2, p1y + p1size.y / 2);
				p1name.setScale({ 2.0f - ofs, 2.0f - ofs });
				wnd->draw(p1name);
				break;

			case State::NAME1_SHAKE:
				ofs = 1.0f - ofs;
				p1name.setPosition(p1x + ofs*Anim::shake(5), p1y + ofs*Anim::shake(5));
				wnd->draw(p1name);
				break;

			case State::VS_ENTER:
				ofs = Anim::paracurve(ofs);
				p1name.setPosition(p1x, p1y);
				vs.setPosition(-vs_size.x + ofs*(vs_size.x + vs_x), vs_y);
				wnd->draw(p1name);
				wnd->draw(vs);
				break;

			case State::VS_HOVER:
				ofs = Anim::paracurve(ofs);
				p1name.setPosition(p1x, p1y);
				vs.setPosition(vs_x, vs_y);
				wnd->draw(p1name);
				wnd->draw(vs);
				break;

			case State::NAME2_FLY:
				ofs *= ofs;
				p2name.setOrigin({ p2size.x / 2, p2size.y / 2 });
				p2name.setPosition(p2x + p2size.x / 2, p2y + p2size.y / 2);
				p2name.setScale({ 2.0f - ofs, 2.0f - ofs });
				p1name.setPosition(p1x, p1y);
				vs.setPosition(vs_x, vs_y);
				wnd->draw(p1name);
				wnd->draw(vs);
				wnd->draw(p2name);
				break;

			case State::NAME2_SHAKE:
				ofs = 1.0f - ofs;
				p1name.setPosition(p1x, p1y);
				vs.setPosition(vs_x, vs_y);
				p2name.setPosition(p2x + ofs*Anim::shake(5), p2y + ofs*Anim::shake(5));
				wnd->draw(p1name);
				wnd->draw(vs);
				wnd->draw(p2name);
				break;

			case State::VS_LEAVE:
				ofs = 1.0f - Anim::paracurve(1.0f - ofs);
				p1name.setPosition(p1x, p1y);
				vs.setPosition(vs_x + ofs*(wndsize.x - vs_x), vs_y);
				p2name.setPosition(p2x, p2y);
				wnd->draw(p1name);
				wnd->draw(vs);
				wnd->draw(p2name);
				break;

			case State::POSITIONS:
			{
				p1name.setScale(1.0f - 0.5f*ofs, 1.0f - 0.5f*ofs);
				p2name.setScale(1.0f - 0.5f*ofs, 1.0f - 0.5f*ofs);
				p1name.setPosition(p1x*(1.0f - ofs), p1y*(1.0f - ofs));
				p2name.setPosition((wndsize.x - (p2size.x / 2))*ofs + p2x*(1.0f - ofs),
					p2y*(1.0f - ofs));
				wnd->draw(p1name);
				wnd->draw(p2name);
			}
				break;

			case State::EXIT:
				p1name.setScale(0.5f, 0.5f);
				p2name.setScale(0.5f, 0.5f);
				p1name.setPosition(0, 0);
				vs.setPosition(vs_x, vs_y);
				p2name.setPosition(wndsize.x - (p2size.x / 2), 0);
				wnd->draw(p1name);
				wnd->draw(p2name);
				break;
		}

		wnd->display();
	}
}
