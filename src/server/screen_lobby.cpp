#include <SFML/Graphics.hpp>
#include "screens.h"
#include "game.h"
#include "common.h"
#include "video.h"
#include "client_interface.h"
#include "globals.h"
#include "resource.h"
#include "gamestate.h"

namespace
{
	class LobbyScreen : public Screens::IScreen
	{
		void on_init() { }
		void process();
		void wnd_event(sf::Event& e);
		void on_enter();
		void on_leave() { }
		void on_shutdown() { }
		Screens::ScreenID get_id() { return Screens::LOBBY; }
	};

	Screens::Register<LobbyScreen> s_do_reg;
}

namespace
{
	void LobbyScreen::on_enter()
	{
		GameState::inst()->music.stop();
	}

	void LobbyScreen::wnd_event(sf::Event& e)
	{
	}

	void LobbyScreen::process()
	{
		if (ClientInterface::Event* e = ClientInterface::get_event())
		{
			using namespace ClientInterface;

			switch (e->type)
			{
				case Event::CONNECT:
				{
					auto ce = (ConnectEvent*)e;
					GameState::inst()->accept_connection(ce);
					Screens::goto_screen(Screens::NAME_INTRO);
					break;
				}
			}

			delete e;
		}

		sf::RenderWindow* wnd = Video::get_window();

		wnd->clear();
		sf::Font& font = Resource::get_default_font();
		sf::Text message("Waiting for connection", font);
		wnd->draw(message);
		wnd->display();
	}
}