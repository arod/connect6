#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <fstream>

#include "video.h"
#include "globals.h"
#include "render.h"
#include "game.h"
#include "client_interface.h"
#include "screens.h"
#include "resource.h"
#include "draw_board.h"
#include "draw_statusbar.h"

int main(int argc, char** argv)
{
	try
	{
		Video::init();
		Resource::init();
		ClientInterface::init(Globals::inst()->server_port);
		Screens::init();

		DrawBoard::init();
	
		if (argc > 1)
		{
			Globals::inst()->server_port = std::stoi(argv[1]);
		}

		// Main loop!
		sf::Window* wnd = Video::get_window();
		sf::Event event;

		while (wnd->isOpen())
		{
			if (ClientInterface::is_idle())
			{
				ClientInterface::listen();
			}

			ClientInterface::process();

			// Event processing.
			while (wnd->pollEvent(event))
			{
				// If window is about to be closed, leave program.
				switch (event.type)
				{
					case sf::Event::Closed:
						wnd->close();
						break;

					default:
						Screens::handle_wnd_event(event);
						break;
				}
			}

			Screens::process();

			sf::sleep(sf::milliseconds(1));
		}

		Screens::shutdown();
		ClientInterface::shutdown();
		Resource::shutdown();
		Video::shutdown();
	}
	catch (std::exception& e)
	{
		std::ofstream ofs("error.txt");
		ofs << e.what();
		ofs.close();
	}

	return 0;
}



