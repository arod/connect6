#pragma once

#include <string>

struct Globals
{
	// Settings and their defaults
	int screen_width = 1024;
	int screen_height = 768;

	unsigned short server_port = 21337;

	// Call this to get the singleton
	static Globals* inst()
	{
		// Get singleton handle
		static Globals the_inst;
		return &the_inst;
	}

protected:
	Globals() = default;
};
