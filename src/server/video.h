#pragma once

namespace sf { class RenderWindow; }
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Text.hpp>

namespace Video
{
	void init();
	sf::RenderWindow* get_window();
	void shutdown();

	void draw_fade(float amt, const sf::Color& c = sf::Color::Black);
	sf::Vector2f text_size(const sf::Text& text);
}
