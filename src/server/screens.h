#pragma once

#include "static_init.h"

namespace sf { class Event; }

namespace Screens
{
	template<class T> using Register = StaticRegistryEntry<T, class IScreen>;

	enum ScreenID
	{
		LOBBY,
		NAME_INTRO,
		BOARD_INTRO,
		INGAME,

		SCREEN_MAX
	};

	class IScreen
	{
	public:
		virtual void on_init() = 0;
		virtual void on_enter() = 0;
		virtual void process() = 0;
		virtual void wnd_event(sf::Event& e) = 0;
		virtual void on_leave() = 0;
		virtual void on_shutdown() = 0;
		virtual ScreenID get_id() = 0;
	};

	void init();
	void shutdown();
	void process();
	void handle_wnd_event(sf::Event& e);
	void goto_screen(ScreenID s);
}
