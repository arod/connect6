#include <SFML/Graphics.hpp>
#include "globals.h"
#include "draw_statusbar.h"
#include "common.h"
#include "video.h"
#include "resource.h"
#include "gamestate.h"

namespace
{
	const int STATUS_NAMES_SIZE = 40;
	const int STATUS_MOVES_SIZE = 50;
	const int STATUS_SPEED_SIZE = 40;
}

void DrawStatusBar::draw()
{
	sf::RenderWindow* wnd = Video::get_window();
	sf::Font& font = Resource::get_default_font();
	GameState* gs = GameState::inst();
	
	int cur_black = gs->cur_black;
	int n_moves = gs->n_moves;

	auto wndsize = wnd->getSize();

	const std::string& blk = gs->player_names[cur_black];
	const std::string& wht = gs->player_names[1 - cur_black];
	std::string moves_str = to_string(n_moves);
	std::string speed_str = "SPD: " + gs->speed_str(gs->gamespeed);

	sf::Text blklabel("Black", font);
	sf::Text whtlabel("White", font);
	sf::Text blkname(blk, font);
	sf::Text whtname(wht, font);
	sf::Text moves(moves_str, font);
	sf::Text speed(speed_str, font);

	blklabel.setCharacterSize(STATUS_NAMES_SIZE);
	whtlabel.setCharacterSize(STATUS_NAMES_SIZE);
	blkname.setCharacterSize(STATUS_NAMES_SIZE);
	whtname.setCharacterSize(STATUS_NAMES_SIZE);
	moves.setCharacterSize(STATUS_MOVES_SIZE);

	auto blksize = Video::text_size(blkname);
	auto whtsize = Video::text_size(whtname);
	auto msize = Video::text_size(moves);
	auto blklabsize = Video::text_size(blklabel);
	auto whtlabsize = Video::text_size(whtlabel);
	auto speedsize = Video::text_size(speed);

	blklabel.setPosition(0, 0);
	whtlabel.setPosition(wndsize.x - whtlabsize.x, 0);
	blkname.setPosition(0, blklabsize.y + 10);
	whtname.setPosition(wndsize.x - whtsize.x, whtlabsize.y + 10);
	moves.setPosition(wndsize.x - msize.x, wndsize.y - speedsize.y - 30);
	speed.setPosition(0, wndsize.y - speedsize.y - 10);

	wnd->draw(blklabel);
	wnd->draw(whtlabel);
	wnd->draw(blkname);
	wnd->draw(whtname);
	wnd->draw(moves);
	wnd->draw(speed);
}