#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <deque>
#include "screens.h"
#include "static_init.h"
#include "game.h"
#include "common.h"
#include "video.h"
#include "client_interface.h"
#include "globals.h"
#include "resource.h"
#include "anim.h"
#include "draw_board.h"
#include "draw_statusbar.h"

namespace
{
	class BoardIntroScreen : public Screens::IScreen
	{
		void on_init();
		void on_shutdown() { }
		void process();
		void wnd_event(sf::Event& e);
		void on_enter();
		void on_leave();
		Screens::ScreenID get_id() { return Screens::BOARD_INTRO; }
	};

	Screens::Register<BoardIntroScreen> s_reg;

	Anim::Timer s_timer;
	
	void process_boardintro();
	void render_boardintro();
	enum class State
	{
		ENTRY,
		BOARD,
		BEGIN_LAND,
		BEGIN_SHAKE,
		EXIT
	} s_cur_state;
}

namespace
{
	//
	// API hooks
	//

	void BoardIntroScreen::on_init()
	{
	}

	void BoardIntroScreen::wnd_event(sf::Event& event)
	{
		if (event.type == sf::Event::KeyPressed)
		{
			s_cur_state = State::EXIT;
		}
	}

	void BoardIntroScreen::on_leave()
	{
	}

	void BoardIntroScreen::on_enter()
	{
		s_cur_state = State::ENTRY;
	}

	void BoardIntroScreen::process()
	{
		process_boardintro();
		render_boardintro();
	}

	//
	// BOARDINTRO
	//

	void process_boardintro()
	{
		switch (s_cur_state)
		{
			case State::ENTRY:
				s_timer.set_for(1000);
				s_cur_state = State::BOARD;
				break;

			case State::BOARD:
				if (s_timer.done())
				{
					s_timer.set_for(200);
					s_cur_state = State::BEGIN_LAND;
				}
				break;

			case State::BEGIN_LAND:
				if (s_timer.done())
				{
					s_timer.set_for(1000);
					s_cur_state = State::BEGIN_SHAKE;
				}
				break;

			case State::BEGIN_SHAKE:
				if (s_timer.done())
				{
					s_timer.set_for(500);
					s_cur_state = State::EXIT;
				}
				break;

			case State::EXIT:
				if (s_timer.done())
				{
					Screens::goto_screen(Screens::INGAME);
				}
				break;
		}
	}

	void render_boardintro()
	{
		sf::RenderWindow* wnd = Video::get_window();
		sf::Font& font = Resource::get_default_font();

		auto wndsize = wnd->getSize();

		sf::Text begin("BEGIN!", font);
		const int BEGIN_SIZE = 120;
		begin.setCharacterSize(BEGIN_SIZE);

		auto textsize = Video::text_size(begin);
		float tx = (wndsize.x - textsize.x) / 2;
		float ty = (wndsize.y - textsize.y) / 2;

		float timer = s_timer.progress();

		wnd->clear();

		switch (s_cur_state)
		{
			case State::BOARD:
				DrawBoard::set_rotation(timer);
				DrawBoard::draw();
				DrawStatusBar::draw();
				break;

			case State::BEGIN_LAND:
				DrawBoard::set_rotation(1.0f);
				DrawBoard::draw();
				DrawStatusBar::draw();
				timer *= timer;
				begin.setOrigin(textsize.x / 2, textsize.y / 2);
				begin.setPosition(tx + textsize.x / 2, ty + textsize.y / 2);
				begin.setScale(3.0f - 2.0f*timer, 3.0f - 2.0f*timer);
				wnd->draw(begin);
				break;

			case State::BEGIN_SHAKE:
				DrawBoard::draw();
				DrawStatusBar::draw();
				timer = 1.0f - timer;
				begin.setPosition(tx + timer*Anim::shake(15), ty + timer*Anim::shake(15));
				wnd->draw(begin);
				break;

			case State::EXIT:
				DrawBoard::set_rotation(1.0f);
				DrawBoard::draw();
				DrawStatusBar::draw();
				break;
		}

		wnd->display();
	}
}
