#include <unordered_map>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include "net_interface.h"

namespace
{
	static const int PROTO_VERSION = 8;

	NetInterface::ServerCommand* parse_start_game(const char* args);

	// Parse the start_game message sent from the server
	NetInterface::ServerCommand* parse_start_game(const char* args)
	{
		int delta;
		int nwalls;

		// Create command
		auto sgc = new NetInterface::StartGameCommand();

		// Scan the start of it to get board size, black player index, and 
		// number of walls. 'delta' captures the offset into the string when
		// sscanf() finishes so we can get walls later.
		int got = sscanf(args, " size %d black %d nwalls %d %n",
			&sgc->board_size, &sgc->black_player, &nwalls, &delta);

		// The %n doesn't count apparently, so we expect scanf() to return 3 things
		if (got != 3)
			throw Exception("Bad start_game command: " + std::string(args));

		// Advance command args position to where the walls actually begin.
		// We expect (nwalls)*2 integers to follow, for each wall's row/col location.
		const char* walls_str = args + delta;

		for (int i = 0; i < nwalls; i++)
		{
			Position wall;
			got = sscanf(walls_str, "%d %d %n", &wall.row, &wall.col, &delta);

			if (got != 2)
			{
				throw Exception("Error parsing walls in start_game command: " + 
					std::string(walls_str));
			}

			sgc->walls.push_back(wall);
			walls_str += delta;
		}

		return sgc;
	}

	NetInterface::ServerCommand* parse_get_move(const char* args)
	{
		return new NetInterface::ServerCommand{NetInterface::ServerCommand::GET_MOVE};
	}

	NetInterface::ServerCommand* parse_disconnect(const char* args)
	{
		return new NetInterface::ServerCommand{NetInterface::ServerCommand::DISCONNECT};
	}
}

void NetInterface::connect(const std::string& host, unsigned short port)
{
	// Resolve hostname
	struct addrinfo* addr_info;
	struct addrinfo hints {0, AF_UNSPEC, SOCK_STREAM, 0};
	int ret = getaddrinfo(
		host.c_str(), 
		to_string((unsigned long long)port).c_str(),
		&hints,
		&addr_info);

	if (ret)
		throw Exception(gai_strerror(ret));

	// Create socket
	m_sock = socket(addr_info->ai_family,
		addr_info->ai_socktype, addr_info->ai_protocol);

	if (m_sock < 0)
		throw PosixException();

	// Connect to host
	ret = ::connect(m_sock, addr_info->ai_addr, addr_info->ai_addrlen);

	if (ret)
		throw PosixException();

	// Cleanup
	freeaddrinfo(addr_info);

	// Initialize buffer
	m_recv_buf.reset();
}

void NetInterface::disconnect()
{
	close(m_sock);
}

void NetInterface::send_init(const std::string& player0, const std::string& player1,
	const std::string& level)
{
	std::string cmd = "hello proto " + to_string(PROTO_VERSION) + 
		" " + player0 + 
		" " + player1 +
		" " + level;

	send_cmd(cmd);
}

void NetInterface::send_disco(int player_idx)
{
	std::string cmd = "disco " + to_string(player_idx);
	send_cmd(cmd);
}

void NetInterface::send_move(int player_idx, const Position& pos, double msec)
{
	std::string cmd = "move " + to_string(player_idx) +
		" " + to_string(pos.row) +
		" " + to_string(pos.col) +
		" " + to_string(msec);

	send_cmd(cmd);
}

NetInterface::ServerCommand* NetInterface::get_command()
{
	while (!m_recv_buf.ready())
	{
		char* buf = m_recv_buf.get_write_ptr();
		int space = m_recv_buf.get_write_space();

		ssize_t bytes = recv(m_sock, buf, space, 0);
		if (bytes < 0)
			throw PosixException();
		else if (bytes == 0)
			throw ConnClosedException();

		m_recv_buf.buffer_data(bytes);
	}

	// The whole command
	const std::string& cmd = m_recv_buf.get_string();

	// Get just the opcode (can be whole string, cmd_len=-1)
	int cmd_len = cmd.find_first_of(' ');
	std::string cmd_code = cmd.substr(0, cmd_len);
	std::string cmd_args = cmd.substr(cmd_len + 1); // skip space

	// Table of handler functions for each command opcode
	typedef ServerCommand* (*HandlerEntry)(const char*);
	static std::unordered_map<std::string, HandlerEntry> handlers = 
	{
		{ "get_move", parse_get_move },
		{ "disconnect", parse_disconnect },
		{ "start_game", parse_start_game }
	};

	ServerCommand* ret = 0;
	
	// Execute the right command, if it exists
	if (handlers.count(cmd_code) > 0)
	{
		auto& cmd_handler = handlers[cmd_code];
		ret = cmd_handler(cmd_args.c_str());
	}
	else
	{
		throw Exception("Unknown server command: " + cmd);
	}		

	m_recv_buf.consume_string();
	return ret;
}

void NetInterface::send_cmd(const std::string& cmd)
{
	const char* buf = cmd.c_str();
	size_t len = cmd.length() + 1; // include null

	while (len > 0)
	{
		ssize_t sent = send(m_sock, buf, len, MSG_NOSIGNAL);
		if (sent == EPIPE)
		{
			throw ConnClosedException();
		}
		else if (sent < 0)
		{
			throw PosixException();
		}
		else
		{
			buf += sent;
			len -= sent;
		}
	}
}
