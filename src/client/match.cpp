#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include "exec_interface.h"
#include "game.h"

namespace
{
	const int TIMEOUT = 1050;

	struct GameResult
	{
		enum Type
		{
			WIN,
			LOSE,
			TIE,
			DISQUALIFY,
			INCOMPLETE
		};

		enum Subtype
		{
			TIMEOUT,
			INVALID_MOVE,
			ERROR
		};

		Subtype subtype;
		Type type;
		int moves;
		double msec;
	};

	struct PlayerInfo
	{
		ExecInterface iface;
		std::string name;
		GameResult results[2];
	};
	
	PlayerInfo s_players[2];
	int s_verbose = 0;
	Game::LevelInfo s_level = { "", 19, "", "" };

	void print_board();
	PlayerColor opp_player(PlayerColor p);
	void do_game(PlayerInfo& p_black, PlayerInfo& p_white, int game_idx);
	void print_results();
	void print_debug(int level, const char* fmt, ...);
}

namespace
{
	void print_debug(int level, const char* fmt, ...)
	{
		if (level > s_verbose)
			return;

		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}

	void print_board()
	{
		using namespace Game;

		if (s_verbose < 3)
			return;

		int size = Game::get_board_size();
		for (int row = 0; row < size; row++)
		{
			for (int col = 0; col < size; col++)
			{
				char c;
				BoardValue val = Game::get_board(row, col);

				switch (val)
				{
					case BoardValue::EMPTY: c = 'U'; break;
					case BoardValue::BLACK: c = 'B'; break;
					case BoardValue::WHITE: c = 'W'; break;
					case BoardValue::WALL: c = 'R'; break;
					default: assert(false);
				}

				printf("%c", c);
			}
			printf("\n");
		}
	}

	PlayerColor opp_player(PlayerColor p)
	{
		return p == P_BLACK ? P_WHITE : P_BLACK;
	}

	void do_game(PlayerInfo& p_black, PlayerInfo& p_white, int game_idx)
	{
		PlayerInfo* players[2] = { &p_black, &p_white };
		GameResult* results[2] = 
		{ 
			&p_black.results[game_idx], 
			&p_white.results[game_idx]
		};

		print_debug(1, "Starting game %d/2: %s (Black) vs. %s (White)\n",
			game_idx+1, p_black.name.c_str(), p_white.name.c_str());

		// Start programs
		for (int i = 0; i < 2; i++)
		{
			try
			{
				players[i]->iface.launch((PlayerColor)i, s_level.size, s_level.walls);
			}
			catch (Exception& e)
			{
				print_debug(1, "%s broke on startup: %s\n",
					players[i]->name.c_str(), e.what());
				results[i]->type = GameResult::DISQUALIFY;
				results[i]->subtype = GameResult::ERROR;
				results[1-i]->type = GameResult::INCOMPLETE;
				return;
			}
		}

		// Initialize internal game state
		Game::init_board(s_level);

		PlayerColor cur_player = P_BLACK;
		int n_moves = 0;
		double total_msec[2] = {0, 0};

		bool done = false;
		while (!done)
		{
			PlayerInfo* pinfo_cur = players[cur_player];

			PlayerColor other_player = opp_player(cur_player);
			PlayerInfo* pinfo_other = players[other_player];

			GameResult* result_cur = &pinfo_cur->results[game_idx];
			GameResult* result_other = &pinfo_other->results[game_idx];

			// Get move from one executable
			ExecInterface::Event ev = pinfo_cur->iface.get_last_event();
			
			if (ev.type != ExecInterface::Event::MOVE)
			{
				std::string err;

				switch (ev.type)
				{
					case ExecInterface::Event::ERROR:
						err = ev.error;
						break;

					case ExecInterface::Event::TIMEOUT:
						err = "did not produce \"Computer moves %c at %d %d\" within 5 seconds";
						break;

					case ExecInterface::Event::SHUTDOWN:
						err = "exited unexpectedly before game has finished";
						break;
				}

				print_debug(1, "%s %s\n", 
					pinfo_cur->name.c_str(), err.c_str());

				print_debug(4, "%s input/output log:\n%s\n",
					pinfo_cur->name.c_str(),
					pinfo_cur->iface.get_io_log().c_str());

				result_cur->type = GameResult::DISQUALIFY;
				result_cur->subtype = GameResult::ERROR;
				result_other->type = GameResult::INCOMPLETE;
				done = true;
				break;
			}

			// Play the move and make sure it's valid
			bool valid = Game::play_move(cur_player, ev.pos);
			if (!valid)
			{
				print_debug(1, "%s played invalid move at (%d %d)\n",
					pinfo_cur->name.c_str(), ev.pos.row, ev.pos.col);
					
				result_cur->type = GameResult::DISQUALIFY;
				result_cur->subtype = GameResult::INVALID_MOVE;
				result_other->type = GameResult::INCOMPLETE;
				done = true;
				break;
			}

			// Check time constraint and print out the valid move
			bool timeout = ev.msec > TIMEOUT;
			print_debug(2, "%s plays %c at (%d %d) in %lfms%s\n",
				pinfo_cur->name.c_str(),
				cur_player == P_WHITE? 'W' : 'B',
				ev.pos.row,
				ev.pos.col,
				ev.msec,
				timeout ? " *TIMED OUT*" : ""
				);

			// Make sure it obeyed the move time constraint
			if (timeout)
			{
				print_debug(1, "%s took too long to make a move (%lf ms)\n",
					pinfo_cur->name.c_str(), ev.msec);
				result_cur->type = GameResult::DISQUALIFY;
				result_cur->subtype = GameResult::TIMEOUT;
				result_other->type = GameResult::INCOMPLETE;
				done = true;
				break;
			}

			print_board();

			// Update statistics
			n_moves++;
			total_msec[cur_player] += ev.msec;

			// Check for endgame
			PlayerColor winner = P_BLACK, loser = P_WHITE;
			switch (Game::get_status())
			{
				case Game::GameStatus::WHITE_WINS:
					winner = P_WHITE; loser = P_BLACK;
				case Game::GameStatus::BLACK_WINS:
				{
					print_debug(1, "%s beats %s in %d moves %s=%lfms %s=%lfms\n",
							players[winner]->name.c_str(),
							players[loser]->name.c_str(),
							n_moves,
							players[winner]->name.c_str(),
							total_msec[winner],
							players[loser]->name.c_str(),
							total_msec[loser]);

					results[winner]->type = GameResult::WIN;
					results[winner]->moves = n_moves;
					results[winner]->msec = total_msec[winner];

					results[loser]->type = GameResult::LOSE;
					results[loser]->moves = n_moves;
					results[loser]->msec = total_msec[winner];

					done = true;
					break;
				}

				case Game::GameStatus::TIE:
				{
					print_debug(1, "tie game %s=%lfms %s=%lfms\n",
							players[0]->name.c_str(),
							total_msec[0],
							players[1]->name.c_str(),
							total_msec[1]);

					results[0]->type = GameResult::TIE;
					results[0]->moves = n_moves;
					results[0]->msec = total_msec[0];

					results[1]->type = GameResult::TIE;
					results[1]->moves = n_moves;
					results[1]->msec = total_msec[1];

					done = true;
					break;
				}

				case Game::GameStatus::IN_PROGRESS:
				{
					try
					{
						// Play the move in the other executable
						pinfo_other->iface.send_move(ev.pos);
					}
					catch (Exception& e)
					{
						print_debug(1, "%s broke after receiving opponent's move: %s\n",
							pinfo_other->name.c_str(), e.what());
						result_other->type = GameResult::DISQUALIFY;
						result_other->subtype = GameResult::ERROR;
						result_cur->type = GameResult::INCOMPLETE;
						done = true;
						break;
					}

					// Trade places
					cur_player = other_player;
					break;
				}
			}
		} // while !done

		// Kill programs
		for (int i = 0; i < 2; i++)
		{
			players[i]->iface.terminate();
		}
	}

	void print_results()
	{
		printf("%s %s ",
			s_players[0].name.c_str(),
			s_players[1].name.c_str()
			);

		for (int game = 0; game < 2; game++)
		{
			for (int p = 0; p < 2; p++)
			{
				GameResult& result = s_players[p].results[game];

				switch (result.type)
				{
					case GameResult::WIN:
						printf("W %d ", result.moves);
						break;

					case GameResult::LOSE:
						printf("L %d ", result.moves);
						break;

					case GameResult::TIE:
						printf("T %lf ", result.msec);
						break;

					case GameResult::DISQUALIFY:
					{
						std::string reason;
						switch (result.subtype)
						{
							case GameResult::ERROR: reason = "ERR"; break;
							case GameResult::INVALID_MOVE: reason = "INVM"; break;
							case GameResult::TIMEOUT: reason = "TOUT"; break;
						}
						printf("Q %s ", reason.c_str());
						break;
					}

					case GameResult::INCOMPLETE:
						printf("N _ ");
						break;
				}
			}
		}
		printf("\n");
	}
}


int main(int argc, char** argv)
{
	if (argc < 5)
	{
		printf("Usage: %s p1_name p1_prog p2_name p2_prog [level file] [verbosity]\n", argv[0]);
		return 1;
	}

	try
	{
		// Initialize players
		s_players[0].name = argv[1];
		s_players[0].iface.set_exe(argv[2]);

		s_players[1].name = argv[3];
		s_players[1].iface.set_exe(argv[4]);

		// Initialize board size, default to largest
		if (argc >= 6)
			s_level = Game::load_level(argv[5]);

		if (argc >= 7)
			s_verbose = atoi(argv[6]);

		// Play two games
		do_game(s_players[0], s_players[1], 0);
		do_game(s_players[1], s_players[0], 1);

		// Print results
		print_results();
	}
	catch (std::exception& e)
	{
		printf("Error: %s\n", e.what());
		return 1;
	}

	return 0;
}
