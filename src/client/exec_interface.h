#pragma once

#include <string>
#include <vector>
#include "common.h"
#include "move_timer.h"
#include "game_types.h"

class ExecInterface
{
public:
	struct Event
	{
		enum Type
		{
			MOVE,
			TIMEOUT,
			SHUTDOWN,
			ERROR
		};

		Type type;
		Position pos;
		double msec;
		std::string error;
	};

	class PipeClosedException : public Exception
	{
	public:
		PipeClosedException() : Exception("Pipe closed") { }
	};

	ExecInterface();
	ExecInterface(const std::string& exe);
	void launch(PlayerColor player, int board_size, const Positions& walls);
	void send_move(const Position& pos);
	const Event& get_last_event();
	void terminate();
	const std::string& get_io_log() { return m_log; }

	PROP_GET_SET(exe, const std::string&, m_exe);

protected:
	static const int IO_TIMEOUT_MS = 5000;

	void send_string(const std::string& str);
	void send_position(const Position&);
	int recv_string(std::string& buf);
	void recv_move();
	void send_walls(int board_size, const Positions&);
	void flush_log();

	std::string m_log;
	std::string m_exe;
	int m_fd;
	int m_pid;
	Event m_last_event;
	MoveTimer m_timer;
};

