#pragma once

#include <stdio.h>
#include "common.h"
#include "string_buffer.h"
#include "game_types.h"

class NetInterface
{
public:
	struct ServerCommand
	{
		enum Type
		{
			START_GAME,
			GET_MOVE,
			DISCONNECT
		};

		ServerCommand(Type _type) : type(_type) { }
		Type type;
	};

	struct StartGameCommand : public ServerCommand
	{
		StartGameCommand() : ServerCommand(START_GAME) { }
		int board_size;
		int black_player;
		Positions walls;
	};

	class ConnClosedException : public Exception
	{
	public:
		ConnClosedException() : Exception("Server closed connection") { }
	};

	void connect(const std::string& host, unsigned short port);
	void send_init(const std::string& player0, const std::string& player1,
		const std::string& level);
	void send_disco(int player_idx);
	void send_move(int player_idx, const Position& pos, double msec);
	ServerCommand* get_command();
	void disconnect();

protected:
	void send_cmd(const std::string& cmd);

	int m_sock;
	StringBuffer m_recv_buf;
};