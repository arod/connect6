#pragma once

namespace
{
	const int TEST_BOARD_SIZE = 15;
	const char TEST_BOARD[] =
		"-------        "
		"-     -        "
		"-     -        "
		"-     -        "
		"-     -        "
		"-     -        "
		"-------        "
		"               "
		"               "
		"               "
		"               "
		"               "
		"               "
		"               "
		"               ";	
}
