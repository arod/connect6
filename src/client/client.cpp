#include <cstdio>
#include <cstdlib>
#include "exec_interface.h"
#include "net_interface.h"

namespace
{
	unsigned short SERVER_PORT = 21337;

	ExecInterface s_players[2];
	NetInterface s_net_iface;
	int s_cur_player;
	int s_black_player;

	bool do_next_move();
	void start_game(NetInterface::StartGameCommand* cmd);
}

namespace
{
	bool do_next_move()
	{
		// Get move from one executable
		ExecInterface::Event ev = s_players[s_cur_player].get_last_event();
	
		switch (ev.type)
		{
			case ExecInterface::Event::MOVE:
				// Send move to server
				s_net_iface.send_move(s_cur_player, ev.pos, ev.msec);
				break;

			default:
				// Various errors
				s_net_iface.send_disco(s_cur_player);
				return false;
		}	

		// Send move to other executable
		int other_player = 1 - s_cur_player;

		try
		{
			s_players[other_player].send_move(ev.pos);
		}
		catch (Exception& e)
		{
			s_net_iface.send_disco(other_player);
			return false;
		}

		s_cur_player = other_player;
		return true;
	}

	void start_game(NetInterface::StartGameCommand* cmd)
	{
		s_players[0].terminate();
		s_players[1].terminate();

		int blkp = cmd->black_player;
		int size = cmd->board_size;
		const Positions& walls = cmd->walls;

		s_black_player = blkp;
		s_cur_player = blkp;

		s_players[blkp].launch(P_BLACK, size, walls);
		s_players[1-blkp].launch(P_WHITE, size, walls);
	}
}


int main(int argc, char** argv)
{
	if (argc < 7)
	{
		printf("Usage: %s server_addr level p1_name p1_prog p2_name p2_prog [server port]\n", argv[0]);
		return 1;
	}

	if (argc > 7)
	{
		SERVER_PORT = atoi(argv[7]);
	}

	try
	{
		std::string server_addr(argv[1]);
		std::string level(argv[2]);
		std::string p1_name(argv[3]);
		std::string p1_prog(argv[4]);
		std::string p2_name(argv[5]);
		std::string p2_prog(argv[6]);
		

		s_players[0].set_exe(p1_prog);
		s_players[1].set_exe(p2_prog);

		NetInterface net_iface;
		s_net_iface.connect(server_addr, SERVER_PORT);
		s_net_iface.send_init(p1_name, p2_name, level);

		bool done = false;
		bool in_game = false;
		while (!done)
		{
			// Blocking wait for server to send a command
			auto cmd = s_net_iface.get_command();
			switch (cmd->type)
			{
				case NetInterface::ServerCommand::GET_MOVE:
					in_game = do_next_move();
					break;

				case NetInterface::ServerCommand::DISCONNECT:
					in_game = false;
					done = true;
					break;

				case NetInterface::ServerCommand::START_GAME:
					start_game((NetInterface::StartGameCommand*)cmd);
					in_game = true;
					break;

				default:
					assert(false);
			}

			delete cmd;
		}

		net_iface.disconnect();
		s_players[0].terminate();
		s_players[1].terminate();
	}
	catch (std::exception& e)
	{
		printf("Error: %s\n", e.what());
		return 1;
	}

	return 0;
}
