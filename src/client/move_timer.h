#pragma once

class MoveTimer
{
public:
	void start();
	void stop();
	double get_msec();

protected:
	double m_start_time;
	double m_end_time;
};
