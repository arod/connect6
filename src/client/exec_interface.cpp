#include <unistd.h>
#include <pty.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <signal.h>
#include "exec_interface.h"
#include "common.h"

ExecInterface::ExecInterface()
: m_pid(-1)
{
}

ExecInterface::ExecInterface(const std::string& exe)
: m_exe(exe), m_pid(-1)
{
}

void ExecInterface::launch(PlayerColor player, int board_size, const Positions& walls)
{
	// Disable SIGPIPE
	sigaction(SIGPIPE, 0, 0);

	// Spawn child process
	m_pid = forkpty(&m_fd, 0, 0, 0);
	if (m_pid < 0)
	{
		throw PosixException();
	}
	else if (m_pid == 0)
	{
		// In child process - launch executable
		int result = execl(m_exe.c_str(), m_exe.c_str(), (char*)0);

		// If this is reached, there is an error. Write to stdout in the hopes of catching it
		if (result < 0)
			printf("%s", strerror(errno));

		// Should never be reached
		assert(false);
	}

	// Initialize IO log
	m_log.clear();

	// Still in master process - send game startup to child
	send_string(to_string((long long int)board_size));
	send_string(player == P_BLACK? "B" : "W");

	// Send blocked positions (walls)
	send_walls(board_size, walls);

	// Black player will move immediately, so record and time the move
	if (player == P_BLACK)
	{
		m_timer.start();
		recv_move();
	}
}

void ExecInterface::send_walls(int board_size, const Positions& walls)
{
	auto it = walls.begin();
	auto it_end = walls.end();
	for ( ; it != it_end; ++it)
	{
		const Position& pos = *it;
		assert(pos.row >= 0 && pos.row < board_size);
		assert(pos.col >= 0 && pos.col < board_size);

		send_position(pos);
	}

	Position endpos;
	endpos.row = -1;
	endpos.col = -1;
	send_position(endpos);
}

void ExecInterface::terminate()
{
	if (m_pid >= 0)
	{
		flush_log();
		close(m_fd);
		kill(m_pid, SIGKILL);
	}
	m_pid = -1;
}

void ExecInterface::flush_log()
{
	// not convinced this is necessary yet
}

const ExecInterface::Event& ExecInterface::get_last_event()
{
	return m_last_event;
}

void ExecInterface::send_move(const Position& pos)
{
	m_timer.start();
	send_position(pos);

	// Get computer's reaction right away so we can time properly
	recv_move();
}

void ExecInterface::send_position(const Position& pos)
{
	// Sends a position. Used for moves and walls
	send_string(to_string((long long int)pos.row) + " " +
		to_string((long long int)pos.col));
}

void ExecInterface::recv_move()
{
	std::string buf;
	bool found = false;
	
	// Keep appending incoming data to the buffer until
	// the buffer contains the computer's playing move, or
	// we time out looking for it, or the program shuts down,
	// or some other error occurs.
	while (!found)
	{
		try
		{
			int bytes = recv_string(buf);
			if (bytes < 0)
			{
				// Timeout reading
				m_last_event.type = Event::TIMEOUT;
				break;
			}
			else if (bytes == 0)
			{
				// End of stream
				m_last_event.type = Event::SHUTDOWN;
				break;
			}
		}
		catch (Exception& e)
		{
			// Some horrible POSIX error
			m_last_event.type = Event::ERROR;
			m_last_event.error = e.what();
			break;
		}

		// Still here? Find beginning of computer movement
		size_t parse_start = buf.find("Computer moves ");

		if (parse_start != buf.npos)
		{
			char which_player;
			int nfound = sscanf(buf.c_str() + parse_start, "Computer moves %c at %d %d",
				&which_player, &m_last_event.pos.row, &m_last_event.pos.col);

			if (nfound == 3)
			{
				found = true;
				m_last_event.type = Event::MOVE;
			}
		}
	}

	// Fill in msec field
	m_timer.stop();
	m_last_event.msec = m_timer.get_msec();
}

int ExecInterface::recv_string(std::string& buf)
{
	// Use select() to see if there's data to read within the timeout value
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(m_fd, &fds);
	
	struct timeval timeout;
	timeout.tv_sec = IO_TIMEOUT_MS / 1000;
	timeout.tv_usec = (IO_TIMEOUT_MS % 1000) * 1000;

	int sresult = select(m_fd+1, &fds, 0, 0, &timeout);
	if (sresult < 0)
	{
		// Unspecified, unspeakable error with select() call
		throw PosixException();
	}
	else if (sresult != 1)
	{
		// Timed out waiting for a string
		return -1;
	}

	static char rdbuf[4096];
	int nread = read(m_fd, rdbuf, sizeof(rdbuf));
	if (nread <= 0)
	{
		// Not strictly correct.
		// nread == 0 means EOF (this is normal when the program terminates)
		// nread < 0 means a bad error.
		//
		// On Linux, -1 is returned when 0 should have been

		// Return value of 0 means end-of-file
		return 0;
	}

	// Append to running IO log
	m_log.append(rdbuf, nread);

	// Append to return value buffer
	buf.append(rdbuf, nread);
	return nread;
}

void ExecInterface::send_string(const std::string& string)
{
	// Write the string (minus null terminator)
	int ret = write(m_fd, string.data(), string.size());
	if (ret < 0)
	{
		// Set pid to -1 since program is no longer running
		m_pid = -1;
		throw PipeClosedException();
	}

	// Write the newline character
	char newline = '\n';

	ret = write(m_fd, &newline, 1);
	if (ret < 0)
	{
		m_pid = -1;
		throw PipeClosedException();
	}
}

