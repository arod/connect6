#include <cstdio>
#include <cstdarg>
#include "exec_interface.h"
#include "game.h"
#include "lab9exercise.h"

namespace
{
	const int TIMEOUT = 1000;

	struct PlayerInfo
	{
		ExecInterface iface;
		std::string name;
	};
	
	PlayerInfo s_players[2];
	int s_board_size;

	char color_to_char(PlayerColor p);
	const char* color_to_str(PlayerColor p);
	void setup_board(Positions* out);
	void print_board();
	PlayerColor opp_player(PlayerColor p);
	void do_game(PlayerInfo& p_black, PlayerInfo& p_white, int game_idx);
	void print_debug(int level, const char* fmt, ...);
}

namespace
{
	// Initializes internal board state to the pre-defined test case
	// in the header file. Returns a list of walls that can be passed
	// to each player's executable.
	void setup_board(Positions* out)
	{		
		s_board_size = TEST_BOARD_SIZE;
		Game::init_board(TEST_BOARD_SIZE);
		out->clear();

		Position pos;
		const char* p = TEST_BOARD;
		
		for (pos.row = 0; pos.row < TEST_BOARD_SIZE; pos.row++)
		{
			for (pos.col = 0; pos.col < TEST_BOARD_SIZE; pos.col++)
			{
				if (*p != ' ')
				{
					Game::make_wall(pos);
					out->push_back(pos);
				}
			
				p++;
			}
		}
		assert(*p == '\0');
	}
	
	void print_board()
	{
		using namespace Game;

		int size = Game::get_board_size();
		for (int row = 0; row < size; row++)
		{
			for (int col = 0; col < size; col++)
			{
				char c;
				BoardValue val = Game::get_board(row, col);
				
				switch(val)
				{
					case BoardValue::EMPTY: c = 'U'; break;
					case BoardValue::BLACK: c = 'B'; break;
					case BoardValue::WHITE: c = 'W'; break;
					case BoardValue::WALL: c = 'R'; break;
					default: assert(false);
				}
				
				printf("%c", c);
			}
			printf("\n");
		}
	}

	PlayerColor opp_player(PlayerColor p)
	{
		return p == P_BLACK ? P_WHITE : P_BLACK;
	}
	
	const char* color_to_str(PlayerColor p)
	{
		return p == P_WHITE? "White" : "Black";
	}
	
	char color_to_char(PlayerColor p)
	{
		return p == P_WHITE? 'W' : 'B';
	}

	void do_game(PlayerInfo& p_black, PlayerInfo& p_white, int game_idx)
	{
		PlayerInfo* players[2] = { &p_black, &p_white };

		// Initialize internal game state
		Positions walls;
		setup_board(&walls);

		// Start programs
		printf("Starting game: %s (Black) vs. %s (White)\n", 
			p_black.name.c_str(), p_white.name.c_str());
		
		for (int i = 0; i < 2; i++)
		{
			try
			{
				players[i]->iface.launch((PlayerColor)i, s_board_size, walls);
			}
			catch (Exception& e)
			{
				throw Exception(players[i]->name + 
					" program broke on startup: " + e.what());
			}
		}
		
		PlayerColor cur_player = P_BLACK;
		int n_moves = 0;
		double total_msec[2] = {0, 0};
		int total_timeouts[2] = {0, 0};

		bool done = false;
		while (!done)
		{		
			PlayerColor other_player = opp_player(cur_player);
			
			PlayerInfo* pinfo_cur = players[cur_player];
			PlayerInfo* pinfo_other = players[other_player];

			// Get move from one executable
			ExecInterface::Event ev = pinfo_cur->iface.get_last_event();

			// Handle various error and timeout conditions
			switch (ev.type)
			{
				case ExecInterface::Event::ERROR:
					throw Exception(pinfo_cur->name + " program broke: " + ev.error);

				case ExecInterface::Event::TIMEOUT:
					throw Exception(pinfo_cur->name + 
						" program did not produce \"Computer moves %c at %d %d\" in the last 5 seconds");

				case ExecInterface::Event::SHUTDOWN:
					throw Exception(pinfo_cur->name +
						" program terminated unexpectedly while trying to get a computer move from it");
			}

			assert(ev.type == ExecInterface::Event::MOVE);			
			
			// Make sure it obeyed the move time constraint
			bool timeout = ev.msec > TIMEOUT;
			if (timeout) total_timeouts[cur_player]++;

			// Play the move and make sure it's valid
			bool valid = Game::play_move(cur_player, ev.pos);
			if (!valid)
			{
				throw Exception(pinfo_cur->name + " plays an invalid move at (" +
					to_string(ev.pos.row) + " " + to_string(ev.pos.col) + ")");
			}

			// Valid move, print it out
			printf("%c plays (%d %d) in %lfms%s\n",
				color_to_char(cur_player),
				ev.pos.row,
				ev.pos.col,
				ev.msec,
				timeout? " *TIMED OUT*" : ""
			);
			
			print_board();

			// Update statistics
			n_moves++;
			total_msec[cur_player] += ev.msec;

			// Play the move in the other executable
			try
			{
				pinfo_other->iface.send_move(ev.pos);
			}
			catch (Exception& e)
			{
				throw Exception(pinfo_other->name +
					" broke when trying to send it a move: " +
					e.what());
			}

			// Check for endgame
			PlayerColor winner = P_BLACK;
			switch (Game::get_status())
			{
				case Game::GameStatus::WHITE_WINS:
					winner = P_WHITE;
				case Game::GameStatus::BLACK_WINS:
				{
					printf("%s wins game in %d moves\n",
						color_to_str(winner),
						n_moves);
						
					done = true;
					break;
				}

				case Game::GameStatus::TIE:
				{
					winner = total_msec[P_BLACK] < total_msec[P_WHITE] ?
						P_BLACK : P_WHITE;
						
					PlayerColor loser = opp_player(winner);
				
					printf("Board is full! %s wins with total time %lfms (%s had %lfms)\n",
						color_to_str(winner), total_msec[winner],
						color_to_str(loser), total_msec[loser]
					);

					done = true;
					break;
				}
			}
			
			// If done playing, print out number of timeouts for this round
			if (done)
			{
				for (int i = 0; i < 2; i++)
				{
					if (total_timeouts[i])
					{
						printf("%s had %d moves that took longer than %dms\n",
							color_to_str((PlayerColor)i),
							total_timeouts[i],
							TIMEOUT
						);
					}
				}
			}
			
			// Trade places
			cur_player = other_player;
		} // while !done

		// Kill programs
		for (int i = 0; i < 2; i++)
		{
			players[i]->iface.terminate();
		}
	} 
}


int main(int argc, char** argv)
{	
	try
	{
		// Parse cmd line args
		if (argc < 3)
		{
			printf("Usage: %s <lab 8 executable> <lab 9 executable>\n", argv[0]);
			return 0;
		}
		
		std::string golden_exe(argv[1]);
		std::string student_exe(argv[2]);
	
		// Initialize players
		s_players[0].name = "Exerciser";
		s_players[0].iface.set_exe(golden_exe);

		s_players[1].name = "Student";
		s_players[1].iface.set_exe(student_exe);

		// Play two games
		do_game(s_players[0], s_players[1], 0);
		do_game(s_players[1], s_players[0], 1);
	}
	catch (std::exception& e)
	{
		printf("%s\n", e.what());
		printf("Log of Lab9 input and output:\n%s\n",
			s_players[1].iface.get_io_log().c_str());
		return 1;
	}

	return 0;
}
