#include <time.h>
#include "move_timer.h"

void MoveTimer::start()
{
	struct timespec start;
	clock_gettime(CLOCK_MONOTONIC, &start);
	m_start_time = start.tv_sec + start.tv_nsec / 1E9;
}

void MoveTimer::stop()
{
	struct timespec end;
	clock_gettime(CLOCK_MONOTONIC, &end);
	m_end_time = end.tv_sec + end.tv_nsec / 1E9;
}

double MoveTimer::get_msec()
{
	return ((m_end_time - m_start_time) * 1000.0);
}
