#pragma once

// Common includes
#include <string>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <sstream>

// Get/set macros for class properties
#define PROP_GET(name,type,field) \
	type get_##name() const { return field; }

#define PROP_SET(name,type,field) \
		void set_##name(type name) { field = name; }

#define PROP_GET_SET(name,type,field) \
		PROP_GET(name, type, field) \
		PROP_SET(name, type, field)

class Exception : public std::runtime_error
{
public:
	Exception(const char* what)
		: std::runtime_error(what) { }
	Exception(const std::string& what)
		: std::runtime_error(what) { }
};

class PosixException : public Exception
{
public:
	PosixException()
		: Exception(strerror(errno)) { }
};

template<class T>
static std::string to_string(const T& val)
{
	//return std::to_string((unsigned long long)val);
	std::stringstream ss;
	ss << val;
	return ss.str();
}
