#pragma once

#include <vector>
#include <string>

enum PlayerColor
{
	P_BLACK = 0,
	P_WHITE = 1
};

struct Position
{
	int row;
	int col;
};

typedef std::vector<Position> Positions;

