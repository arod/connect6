#pragma once

#include <deque>
#include <string>

// This buffers incoming string data from the network, up to the first null.
// Then it becomes 'ready' and other code can access the stored string.
class StringBuffer
{
public:
	StringBuffer(int size = 1024); // max size of any ONE string
	~StringBuffer();

	void reset();

	// For reading
	bool ready();
	const std::string& get_string();	
	void consume_string();

	// For writing
	char* get_write_ptr();
	int get_write_space();
	void buffer_data(int size);

private:
	typedef std::deque<std::string> StringQueue;

	StringQueue m_queue;
	char* m_buffer;
	int m_write_pos;
	int m_buf_size;
};
