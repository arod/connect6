#include <cstring>
#include <cstdio>
#include <cassert>
#include "string_buffer.h"

StringBuffer::StringBuffer(int size)
{
	m_buffer = new char[size];
	m_buf_size = size;
	reset();
}

StringBuffer::~StringBuffer()
{
	delete m_buffer;
}

void StringBuffer::reset()
{
	m_write_pos = 0;
	m_queue.clear();
}

const std::string& StringBuffer::get_string()
{
	assert(!m_queue.empty());
	return m_queue.front();
}

bool StringBuffer::ready()
{
	return !m_queue.empty();
}

char* StringBuffer::get_write_ptr()
{
	return m_buffer + m_write_pos;
}

int StringBuffer::get_write_space()
{
	return m_buf_size - m_write_pos;
}

void StringBuffer::buffer_data(int len)
{
	// Pointer to start of new data
	char* scan_start = m_buffer + m_write_pos;
	
	// Pointer to start of next (complete or incomplete) string
	char* str_start = m_buffer;

	char* zero = 0;
	while((zero = (char*)memchr(scan_start, '\0', len)))
	{
		// Size of string, excludes null
		int str_size = zero - str_start;

		// Add it to our queue of completed strings
		m_queue.emplace_back(str_start, str_size);

		// Advance scan pointer and string pointer to after the null
		scan_start = zero + 1;
		str_start = zero + 1;

		// Consume null as well
		len -= str_size + 1;
	}

	// Found at least one string?
	if (zero)
	{
		// Copy remainder to start of buffer
		memcpy(m_buffer, scan_start, len);

		// Keep write pointer pointing to after the remainder
		m_write_pos = len;
	}
	else
	{
		// Just advance the write pointer
		m_write_pos += len;
	}

	assert(m_write_pos <= m_buf_size);
}

void StringBuffer::consume_string()
{
	assert(!m_queue.empty());
	m_queue.pop_front();
}
