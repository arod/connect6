#pragma once

#include "game_types.h"

namespace Game
{
	enum class BoardValue
	{
		EMPTY,
		BLACK,
		WHITE,
		WALL
	};

	enum class GameStatus
	{
		IN_PROGRESS,
		BLACK_WINS,
		WHITE_WINS,
		TIE
	};

	struct LevelInfo
	{
		std::string name;
		int size;
		std::string tileset;
		std::string track;
		Positions walls;
	};

	void init_board(int size);
	void init_board(const LevelInfo&);
	bool play_move(PlayerColor player, const Position& pos);
	bool is_valid_move(PlayerColor player, const Position& pos);
	void make_wall(const Position& pos);
	BoardValue get_board(int row, int col);
	BoardValue get_board(const Position& pos);
	GameStatus get_status();
	int get_board_size();
	LevelInfo load_level(const std::string& filename);
}