#include <algorithm>
#include <cstdio>
#include <unordered_map>
#include "common.h"
#include "game.h"

using namespace Game;

namespace
{
	BoardValue* s_board = 0;
	int s_size;

	bool board_is_full();
	GameStatus status_from_winner(PlayerColor p);
	bool check6(int startx, int starty, int dx, int dy, PlayerColor* out_winner);
	void set_board(int row, int col, BoardValue val);
	void set_board(const Position& pos, BoardValue val);
	bool valid_coords(int row, int col);
	bool is_player(BoardValue value);

	bool is_player(BoardValue value)
	{
		return value == BoardValue::WHITE || value == BoardValue::BLACK;
	}

	void set_board(int row, int col, BoardValue val)
	{
		s_board[row*s_size+col] = val;
	}

	void set_board(const Position& pos, BoardValue val)
	{
		set_board(pos.row, pos.col, val);
	}

	bool board_is_full()
	{
		for (int i = 0; i < s_size * s_size; i++)
		{
			if (s_board[i] == BoardValue::EMPTY)
			{
				return false;
				break;
			}
		}
		return true;
	}

	GameStatus status_from_winner(PlayerColor p)
	{
		if (p == P_BLACK) return GameStatus::BLACK_WINS;
		else if (p == P_WHITE) return GameStatus::WHITE_WINS;
		else throw Exception("Invalid player color");
		
		return GameStatus::BLACK_WINS;
	}

	bool valid_coords(int row, int col)
	{
		return row >= 0 && col >= 0 && row < s_size && col < s_size;
	}

	bool check6(int startx, int starty, int dx, int dy, PlayerColor* out_winner)
	{
		// Start checking at startx,starty
		int x = startx;
		int y = starty;
		BoardValue last = BoardValue::EMPTY; // last value seen
		int count = 0; // how many consecutive we've found

		// Continue checking until we run off the board
		while (valid_coords(y, x))
		{
			// Row=y, Col=x
			BoardValue cur = get_board(y,x);

			// Continuation of black/white chain? Otherwise, reset
			// chain to 1 (just this square)
			if (is_player(cur) && cur == last)
				count++;
			else
				count = 1;

			// Check for win and set winner
			if (count == 6)
			{
				assert(is_player(last));
				*out_winner = (last == BoardValue::BLACK) ? P_BLACK : P_WHITE;
				return true;
			}

			// Advance current checking position
			last = cur;
			x += dx;
			y += dy;
		}

		// Went off board without finding a 6-streak of anything
		return false;
	}
}

void Game::init_board(int size)
{
	// Initialize emty board
	if (s_board)
		delete[] s_board;

	s_board = new BoardValue[size*size];
	s_size = size;
	std::fill_n(s_board, size*size, BoardValue::EMPTY); 
}

void Game::init_board(const LevelInfo& info)
{
	// Initialize board from levelinfo, with walls
	init_board(info.size);

	for (auto it = info.walls.begin(); it != info.walls.end(); ++it)
		make_wall(*it);
}

bool Game::play_move(PlayerColor player, const Position& pos)
{
	if (!is_valid_move(player, pos))
		return false;
	
	BoardValue val = player == P_BLACK ? BoardValue::BLACK : BoardValue::WHITE;
	set_board(pos, val);
	return true;
}

bool Game::is_valid_move(PlayerColor player, const Position& pos)
{
	if (!valid_coords(pos.row, pos.col))
		return false;

	if (get_board(pos) != BoardValue::EMPTY)
		return false;

	return true;
}

BoardValue Game::get_board(int row, int col)
{
	return s_board[row*s_size + col];
}

BoardValue Game::get_board(const Position& pos)
{
	return get_board(pos.row, pos.col);
}

int Game::get_board_size()
{
	return s_size;
}

void Game::make_wall(const Position& pos)
{
	set_board(pos.row, pos.col, BoardValue::WALL);
}

Game::GameStatus Game::get_status()
{
	if (board_is_full())
		return GameStatus::TIE;

	// Now check for 6-matches
	PlayerColor winner;

	// Check for a 6 within each row
	for (int row = 0; row < s_size; row++)
	{
		// Start from y=row, x=0, traveling rightwards
		if (check6(0, row, 1, 0, &winner))
			return status_from_winner(winner);
	}

	// Check for a 6 within each column
	for (int col = 0; col < s_size; col++)
	{
		// Start from y=0, x=row, traveling downwards
		if (check6(col, 0, 0, 1, &winner))
			return status_from_winner(winner);
	}

	// Travel along all the squares on the top and right edges
	// and check for 6es within SW-pointing diagonals
	for (int i = 0; i < 2*s_size - 1; i++)
	{
		int startx = std::min(i, s_size-1);
		int starty = std::max(0, i - s_size);
		if (check6(startx, starty, -1, 1, &winner))
			return status_from_winner(winner);

		// While we're here, do the same thing for the BOTTOM and
		// RIGHT edges with NW-pointing diagonals
		starty = s_size-1 - starty;
		if (check6(startx, starty, -1, -1, &winner))
			return status_from_winner(winner);
	}

	// No winner or tie, so game is still in progress
	return GameStatus::IN_PROGRESS;
}

LevelInfo Game::load_level(const std::string& filename)
{
	// Open file and parse "key:value" pairs.
	// Multiple entries for one key allowed (values get string-appended)
	FILE* fp = fopen(filename.c_str(), "r");
	if (!fp)
		throw PosixException();

	std::unordered_map<std::string, std::string> keyval;

	while (!feof(fp))
	{
		char line[1024];
		char key[512];
		char val[512];

		if (!fgets(line, sizeof(line), fp))
			break;

		int nread = sscanf(line, "%511s : %511s", key, val);
		if (nread < 2)
		{
			fclose(fp);
			throw Exception("Bad line in level data: " + std::string(line));
		}

		keyval[key] += std::string(val);
	}

	fclose(fp);

	// Now parse key:value pairs
	LevelInfo result;

	result.size = atoi(keyval["size"].c_str());
	result.name = keyval["name"];
	result.tileset = keyval["tileset"];
	result.track = keyval["track"];

	// Parse walls - a bunch of U and R concatenated into a string
	const std::string& walls = keyval["walls"];

	auto walls_it = walls.begin();
	Position pos;
	for (pos.row = 0; pos.row < result.size; pos.row++)
	{
		for (pos.col = 0; pos.col < result.size; pos.col++)
		{
			if (walls_it == walls.end())
				throw Exception("Not enough characters in walls array in level file");

			char c = *walls_it;

			switch (c)
			{
				case 'R': result.walls.push_back(pos); break;
				case 'U': break;
				default: throw Exception("Unexpected character in walls array in level file: "+ std::string(1, c)); break;
			}

			++walls_it;
		}
	}

	return result;
}
